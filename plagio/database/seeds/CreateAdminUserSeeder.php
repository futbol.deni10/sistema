<?php

use Illuminate\Database\Seeder;
use App\User;

use Spatie\Permission\Models\Role;

use Spatie\Permission\Models\Permission;
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => 'admin', 
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'baja' => 'NO', 
            'disponible' => 'NO', 
            'nombre' =>  "Admin-Nombre",
            'apellido' =>  "Admin-Apellido",
            'ci' =>  "7894561",
            'direccion' =>  "Gabriel Rene Moreno",
            'telefono' =>  "+59170048511",
            'ciudad_id' => 4,
        ]);
        $role = Role::create(['name' => 'Administrador','descripcion'=>'Super Admin, tiene todos los permisos en el sistema']);   
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        $dataTipoArchivo = [
            ['nombre' => 'IMAGENES',
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()]
            ,
            ['nombre' => 'DOCUMENTOS',
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()]
        ];
        $dataExtension = [
            ['extension' => 'JPG',
            'estado'=>'activo',
            'tiposarchivo_id'=>1,
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            ['extension' => 'PNG',
            'estado'=>'activo',
            'tiposarchivo_id'=>1,
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            ['extension' => 'JPEG',
            'estado'=>'activo',
            'tiposarchivo_id'=>1,
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            ['extension' => 'DOCX',
            'estado'=>'activo',
            'tiposarchivo_id'=>2,
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            ['extension' => 'PDF',
            'estado'=>'activo',
            'tiposarchivo_id'=>2,
            'user_id'=>1,
            'created_at' => now(),
            'updated_at' => now()
            ],

        ];

        DB::table('tiposarchivo')->insert($dataTipoArchivo);
        DB::table('extension')->insert($dataExtension);

        $role = Role::create(['name' => 'Guest','descripcion'=>'Rol usado para el modulo proyectos TIPO : invitado , tiene solo el permiso para ver proyectos']); 
        $permissions = Permission::where('id',36+8)->orWhere('id',40+8)->pluck('id','id');
        $role->syncPermissions($permissions);
        // $permissions = Permission::where('id',40)->pluck('id','id');
        // $role->syncPermissions($permissions);

        $role = Role::create(['name' => 'Reporter', 'descripcion'=>'Rol usado para el modulo proyectos TIPO : Reporter , tiene el permiso para ver y editar proyectos']); 
        $permissions = Permission::where('id',36+8)
                                    ->orWhere('id',37+8)
                                    ->orWhere('id',40+8)
                                    ->orWhere('id',41+8)->pluck('id','id');
        $role->syncPermissions($permissions);

        $role = Role::create(['name' => 'Developer','descripcion'=>'Rol usado para el modulo proyectos TIPO : Developer , tiene el permiso para ver ,editar y actualizar proyectos']); 
        $permissions = Permission::where('id',36+8)
                                    ->orWhere('id',37+8)
                                    ->orWhere('id',38+8)
                                    //->orWhere('id',39+8)
                                    ->orWhere('id',40+8)
                                    ->orWhere('id',41+8)
                                    ->orWhere('id',42+8)->pluck('id','id');
        $role->syncPermissions($permissions);

        $role = Role::create(['name' => 'JefeProyecto','descripcion'=>'Rol usado para el modulo proyectos TIPO : JefeProyecto , tiene el permiso para ver ,editar, actualizar y eliminar proyectos']); 
        $permissions = Permission::where('id','>=',32+8)->where('id','<=',43+8)->pluck('id','id');
        $role->syncPermissions($permissions);
    }
}
