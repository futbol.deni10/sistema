<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{

    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-create',
           'user-edit',
           'user-delete',
           // 'persona-list',
           // 'persona-create',
           // 'persona-edit',
           // 'persona-delete',
           'usuariobloqueado-list',
           'usuariobloqueado-create',
           'usuariobloqueado-edit',
           'usuariobloqueado-delete',
           'GestorArchivo-list',
           'GestorArchivo-create',
           'GestorArchivo-edit',
           'GestorArchivo-delete',
           'AlmacenarArchivo-list',
           'AlmacenarArchivo-create',
           'AlmacenarArchivo-edit',
           'AlmacenarArchivo-delete',
           'CompararArchivo-list',
           'CompararArchivo-create',
           'CompararArchivo-edit',
           'CompararArchivo-delete',
           'CompararOcr-list',
           'CompararOcr-create',
           'CompararOcr-edit',
           'CompararOcr-delete',
           'ResultadosArchivo-list',
           'ResultadosArchivo-create',
           'ResultadosArchivo-edit',
           'ResultadosArchivo-delete',

           'CategoriaProyecto-list',
           'CategoriaProyecto-create',
           'CategoriaProyecto-edit',

           'GestorProyecto-list',
           'GestorProyecto-create',
           'GestorProyecto-edit',
           'GestorProyecto-delete',

           'AsignarUsuario-list',
           'AsignarUsuario-create',
           'AsignarUsuario-edit',
           'AsignarUsuario-delete',

           'GestorArchivoProyecto-list',
           'GestorArchivoProyecto-create',
           'GestorArchivoProyecto-edit',
           'GestorArchivoProyecto-delete',

           'GestionVersiones-list',
           'GestionVersiones-create',
           'GestionVersiones-edit',
           'GestionVersiones-delete',

           'Redaccion-list',
           'Redaccion-create',
           'Redaccion-edit',
           'Redaccion-delete',
        ];
        
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }

        $dataCiudad = [
          ['nombre'=>'CHUQUISACA','abreviacion'=>'CHQ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'LA PAZ','abreviacion'=>'LPZ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'COCHABAMBA','abreviacion'=>'CBB','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'ORURO','abreviacion'=>'ORU','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'POTOSI','abreviacion'=>'PTS','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'TARIJA','abreviacion'=>'TAR','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'SANTA CRUZ','abreviacion'=>'SCZ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'BENI','abreviacion'=>'BEN','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'PANDO','abreviacion'=>'PAN','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'OTROS','abreviacion'=>'OTR','created_at'=>now(),'updated_at'=>now()],
        ];

        DB::table('ciudades')->insert($dataCiudad);
    }
}
