-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-06-2021 a las 18:50:59
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `plagio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivosproyecto`
--

CREATE TABLE `archivosproyecto` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proyecto_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `notas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inicio',
  `resultadocomprar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `archivosproyecto`
--

INSERT INTO `archivosproyecto` (`id`, `archivo`, `proyecto_id`, `user_id`, `version`, `notas`, `resultadocomprar`, `created_at`, `updated_at`) VALUES
(2, 'QWWW', 1, 1, '1', 'inicio', NULL, '2021-06-08 19:53:09', '2021-06-08 19:53:09'),
(3, 'QWWW', 1, 1, '2', '4597841', 'N;', '2021-06-13 13:24:15', '2021-06-13 13:24:15'),
(8, 'QWWW', 1, 1, '3', '74846', 'a:1:{i:0;a:3:{s:4:\"tipo\";s:7:\"archivo\";s:7:\"mensaje\";s:26:\"No se encuentra el archivo\";s:9:\"comparado\";s:27:\"QWWW/version2/Archivo1.docx\";}}', '2021-06-13 15:57:53', '2021-06-13 15:57:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

CREATE TABLE `asignaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activo',
  `tiempo` date DEFAULT NULL,
  `proyecto_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `creadopor` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `asignaciones`
--

INSERT INTO `asignaciones` (`id`, `estado`, `tiempo`, `proyecto_id`, `user_id`, `creadopor`, `created_at`, `updated_at`) VALUES
(1, 'activo', NULL, 1, 1, 1, '2021-06-08 18:48:02', '2021-06-08 18:48:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bloqueos`
--

CREATE TABLE `bloqueos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `razon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiempo` date DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `creadopor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_proyecto`
--

CREATE TABLE `categoria_proyecto` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categoria_proyecto`
--

INSERT INTO `categoria_proyecto` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'qwe', '2021-06-08 18:47:52', '2021-06-08 18:47:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abreviacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `nombre`, `abreviacion`, `created_at`, `updated_at`) VALUES
(1, 'CHUQUISACA', 'CHQ', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(2, 'LA PAZ', 'LPZ', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(3, 'COCHABAMBA', 'CBB', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(4, 'ORURO', 'ORU', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(5, 'POTOSI', 'PTS', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(6, 'TARIJA', 'TAR', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(7, 'SANTA CRUZ', 'SCZ', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(8, 'BENI', 'BEN', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(9, 'PANDO', 'PAN', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(10, 'OTROS', 'OTR', '2021-06-05 04:51:03', '2021-06-05 04:51:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extension`
--

CREATE TABLE `extension` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactivo',
  `tiposarchivo_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `extension`
--

INSERT INTO `extension` (`id`, `extension`, `estado`, `tiposarchivo_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'JPG', 'activo', 1, 1, '2021-06-06 03:29:54', '2021-06-06 03:29:54'),
(2, 'PDF', 'activo', 2, 1, '2021-06-08 21:16:18', '2021-06-08 21:16:18'),
(3, 'DOCX', 'activo', 2, 1, '2021-06-08 21:16:24', '2021-06-08 21:16:24'),
(4, 'PNG', 'activo', 1, 1, '2021-06-08 21:16:29', '2021-06-08 21:16:29'),
(5, 'JPEG', 'activo', 1, 1, '2021-06-08 21:16:34', '2021-06-08 21:16:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_06_10_000001_users_table', 1),
(2, '2020_06_10_000002_create_permission_tables', 1),
(3, '2020_06_16_152624_table_ciudades', 1),
(4, '2020_06_16_152651_table_personas', 1),
(5, '2021_01_22_190319_tabla_tiposarchivo', 1),
(6, '2021_01_22_190329_tabla_extension', 1),
(7, '2021_01_22_190345_tabla_repositorio_archivo', 1),
(8, '2021_02_21_035613_tabla_categoria_proyecto', 1),
(9, '2021_02_21_035713_tabla_proyecto', 1),
(10, '2021_02_21_122546_table_roleproyecto', 1),
(11, '2021_02_21_122646_tabla_asignacion', 1),
(12, '2021_02_22_024900_tabla_archivosproyecto', 1),
(13, '2021_02_28_024146_table_personalizar', 1),
(14, '2021_03_05_020914_create_jobs_table', 1),
(15, '2021_03_05_024314_tabla_resultado_comparar', 1),
(16, '2021_05_28_163439_tabla_bloqueousuario', 1),
(17, '2021_06_04_212154_tabla_redaccion', 1),
(18, '2021_03_05_020924_create_jobs_table', 2),
(19, '0000_00_00_000000_create_websockets_statistics_entries_table', 3),
(20, '2021_06_13_002556_table_res_comparar_proye', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(5, 'App\\User', 1),
(6, 'App\\User', 2),
(6, 'App\\User', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(2, 'role-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(3, 'role-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(4, 'role-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(5, 'user-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(6, 'user-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(7, 'user-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(8, 'user-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(9, 'persona-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(10, 'persona-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(11, 'persona-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(12, 'persona-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(13, 'usuariobloqueado-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(14, 'usuariobloqueado-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(15, 'usuariobloqueado-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(16, 'usuariobloqueado-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(17, 'GestorArchivo-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(18, 'GestorArchivo-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(19, 'GestorArchivo-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(20, 'GestorArchivo-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(21, 'AlmacenarArchivo-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(22, 'AlmacenarArchivo-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(23, 'AlmacenarArchivo-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(24, 'AlmacenarArchivo-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(25, 'CompararArchivo-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(26, 'CompararArchivo-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(27, 'CompararArchivo-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(28, 'CompararArchivo-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(29, 'CompararOcr-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(30, 'CompararOcr-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(31, 'CompararOcr-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(32, 'CompararOcr-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(33, 'ResultadosArchivo-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(34, 'ResultadosArchivo-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(35, 'ResultadosArchivo-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(36, 'ResultadosArchivo-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(37, 'CategoriaProyecto-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(38, 'CategoriaProyecto-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(39, 'CategoriaProyecto-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(40, 'GestorProyecto-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(41, 'GestorProyecto-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(42, 'GestorProyecto-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(43, 'GestorProyecto-delete', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(44, 'AsignarUsuario-list', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(45, 'AsignarUsuario-create', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(46, 'AsignarUsuario-edit', 'web', '2021-06-05 04:51:02', '2021-06-05 04:51:02'),
(47, 'AsignarUsuario-delete', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(48, 'GestorArchivoProyecto-list', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(49, 'GestorArchivoProyecto-create', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(50, 'GestorArchivoProyecto-edit', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(51, 'GestorArchivoProyecto-delete', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(52, 'GestionVersiones-list', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(53, 'GestionVersiones-create', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(54, 'GestionVersiones-edit', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(55, 'GestionVersiones-delete', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(56, 'Redaccion-list', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(57, 'Redaccion-create', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(58, 'Redaccion-edit', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(59, 'Redaccion-delete', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personalizados`
--

CREATE TABLE `personalizados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pcoded_navbar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navbar_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcoded_header` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_item_theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcoded_navigatio_lavel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nav_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vertical_effect` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_border_style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropdown_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subitem_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ci` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `apellido`, `ci`, `direccion`, `telefono`, `movil`, `ciudad_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'EQWE', 'QWEQWEQW', '12312312', '312312312', '+59131231312', NULL, 3, 3, '2021-06-13 16:36:24', '2021-06-13 16:36:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_proyecto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baja` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `categoria_proyecto_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `nombre`, `descripcion`, `tipo_proyecto`, `baja`, `categoria_proyecto_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'QWWW', '222', 'Privado', 'NO', 1, 1, '2021-06-08 18:48:01', '2021-06-08 18:48:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redaccion`
--

CREATE TABLE `redaccion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asignados` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creadopor` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `redaccion`
--

INSERT INTO `redaccion` (`id`, `nombre`, `contenido`, `asignados`, `creadopor`, `created_at`, `updated_at`) VALUES
(4, 'PRUEBA', '<p>12344554789741</p><p>12345678</p><p>12345678</p>', 'a:3:{i:0;s:1:\"3\";i:1;s:1:\"1\";i:2;s:1:\"2\";}', 1, '2021-06-05 08:24:08', '2021-06-08 21:35:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repositorio_archivo`
--

CREATE TABLE `repositorio_archivo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `md5_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ocr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `extension_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `repositorio_archivo`
--

INSERT INTO `repositorio_archivo` (`id`, `url`, `md5_file`, `estado`, `ocr`, `extension_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Archivo1.jpg', 'b281eee0be373695109a669ca764ba55', 'archivo', 'NO', 1, 1, '2021-06-06 03:30:12', '2021-06-06 03:30:12'),
(2, 'Archivo1.jpg', 'b281eee0be373695109a669ca764ba55', 'comparar', 'NO', 1, 1, '2021-06-06 03:30:21', '2021-06-06 03:30:21'),
(3, 'Archivo1.docx', '0e709e91dc8a2e3d00c62d1a98a2544d', 'archivo', 'NO', 3, 1, '2021-06-08 21:16:41', '2021-06-08 21:16:41'),
(4, 'Archivo1.png', '07fbddccab58286f39a6aedb1b61613a', 'archivo', 'NO', 4, 1, '2021-06-08 21:16:48', '2021-06-08 21:16:48'),
(5, 'Archivo2.jpg', '5e12e986f8ccb262d12f65d1ff46f691', 'comparar', 'NO', 1, 1, '2021-06-08 21:16:55', '2021-06-08 21:16:55'),
(6, 'Archivo1.jpeg', 'a29abdf7f5dabdeb0963d6c70ff5c1c5', 'comparar', 'NO', 5, 1, '2021-06-08 21:17:04', '2021-06-08 21:17:04'),
(7, 'Archivo1.jpeg', '1d3062a5967357538d546f21eb8c303f', 'archivo', 'NO', 5, 1, '2021-06-09 02:46:42', '2021-06-09 02:46:42'),
(8, '1.jpeg', '1d3062a5967357538d546f21eb8c303f', 'comparar', 'SI', 5, 1, '2021-06-09 15:03:23', '2021-06-09 15:03:23'),
(9, '450_1000-min.jpeg', 'a29abdf7f5dabdeb0963d6c70ff5c1c5', 'comparar', 'SI', 5, 1, '2021-06-09 18:04:36', '2021-06-09 18:04:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados_comparar`
--

CREATE TABLE `resultados_comparar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `a_comparados` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivosproyecto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `resultados_comparar`
--

INSERT INTO `resultados_comparar` (`id`, `a_comparados`, `archivosproyecto_id`, `created_at`, `updated_at`) VALUES
(1, 'a:1:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1622950224_Archivo1.jpg\";}}', 2, '2021-06-06 03:30:24', '2021-06-06 03:30:24'),
(2, 'a:1:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1622950269_Archivo1.jpg\";}}', 2, '2021-06-06 03:31:09', '2021-06-06 03:31:09'),
(3, 'a:1:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1622950282_Archivo1.jpg\";}}', 2, '2021-06-06 03:31:22', '2021-06-06 03:31:22'),
(4, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187041_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187041_Archivo1.png\";}}', 2, '2021-06-08 21:17:22', '2021-06-08 21:17:22'),
(5, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:66.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187042_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:5.21;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187042_Archivo1.png\";}}', 5, '2021-06-08 21:17:23', '2021-06-08 21:17:23'),
(6, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:41.23;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187043_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:48.11;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187043_Archivo1.png\";}}', 6, '2021-06-08 21:17:23', '2021-06-08 21:17:23'),
(7, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:66.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187090_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:5.21;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187091_Archivo1.png\";}}', 5, '2021-06-08 21:18:11', '2021-06-08 21:18:11'),
(8, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187122_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187122_Archivo1.png\";}}', 2, '2021-06-08 21:18:43', '2021-06-08 21:18:43'),
(9, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:66.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187123_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:5.21;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187123_Archivo1.png\";}}', 5, '2021-06-08 21:18:43', '2021-06-08 21:18:43'),
(10, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187176_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:24.75;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187176_Archivo1.png\";}}', 2, '2021-06-08 21:19:36', '2021-06-08 21:19:36'),
(11, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:76.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187180_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:73.95;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187180_Archivo1.png\";}}', 5, '2021-06-08 21:19:41', '2021-06-08 21:19:41'),
(12, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187184_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:24.75;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187185_Archivo1.png\";}}', 2, '2021-06-08 21:19:45', '2021-06-08 21:19:45'),
(13, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:76.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187189_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:73.95;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187189_Archivo1.png\";}}', 5, '2021-06-08 21:19:49', '2021-06-08 21:19:49'),
(14, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187341_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187342_Archivo1.png\";}}', 2, '2021-06-08 21:22:22', '2021-06-08 21:22:22'),
(15, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187721_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:24.75;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187722_Archivo1.png\";}}', 2, '2021-06-08 21:28:42', '2021-06-08 21:28:42'),
(16, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:76.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187725_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:73.95;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187726_Archivo1.png\";}}', 5, '2021-06-08 21:28:46', '2021-06-08 21:28:46'),
(17, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:51.23;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187729_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:96.06;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187730_Archivo1.png\";}}', 6, '2021-06-08 21:28:50', '2021-06-08 21:28:50'),
(18, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187949_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo1.jpg\";s:12:\"urlResultado\";s:23:\"1623187950_Archivo1.png\";}}', 2, '2021-06-08 21:32:30', '2021-06-08 21:32:30'),
(19, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:66.12;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187950_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:5.21;s:11:\"urlComparar\";s:42:\"/archivocomparar/IMAGENES/JPG/Archivo2.jpg\";s:12:\"urlResultado\";s:23:\"1623187951_Archivo1.png\";}}', 5, '2021-06-08 21:32:31', '2021-06-08 21:32:31'),
(20, 'a:2:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:41.23;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187951_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:48.11;s:11:\"urlComparar\";s:44:\"/archivocomparar/IMAGENES/JPEG/Archivo1.jpeg\";s:12:\"urlResultado\";s:23:\"1623187951_Archivo1.png\";}}', 6, '2021-06-08 21:32:32', '2021-06-08 21:32:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `res_comp_proyecto`
--

CREATE TABLE `res_comp_proyecto` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `a_comparados` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proyectos_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `res_comp_proyecto`
--

INSERT INTO `res_comp_proyecto` (`id`, `a_comparados`, `archivo`, `extension`, `proyectos_id`, `created_at`, `updated_at`) VALUES
(1, 'a:1:{i:0;a:3:{s:3:\"url\";s:13:\"Archivo1.docx\";s:2:\"id\";i:3;s:10:\"porcentaje\";d:100;}}', 'p.docx', 'DOCX', 1, '2021-06-13 05:32:47', '2021-06-13 05:32:47'),
(4, 'a:3:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";d:17.12;s:11:\"urlComparar\";s:33:\"/proyectos2/QWWW/version1/im1.jpg\";s:12:\"urlResultado\";s:23:\"1623563133_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:84.21;s:11:\"urlComparar\";s:33:\"/proyectos2/QWWW/version1/im1.jpg\";s:12:\"urlResultado\";s:23:\"1623563134_Archivo1.png\";}i:2;a:6:{s:2:\"id\";i:7;s:14:\"urlRepositorio\";s:13:\"Archivo1.jpeg\";s:9:\"extension\";s:4:\"JPEG\";s:10:\"porcentaje\";d:58.35;s:11:\"urlComparar\";s:33:\"/proyectos2/QWWW/version1/im1.jpg\";s:12:\"urlResultado\";s:24:\"1623563134_Archivo1.jpeg\";}}', 'im.jpg', 'JPG', 1, '2021-06-13 05:45:34', '2021-06-13 05:45:34'),
(5, 'a:1:{i:0;a:3:{s:3:\"url\";s:13:\"Archivo1.docx\";s:2:\"id\";i:3;s:10:\"porcentaje\";d:10.031790886612505;}}', '1.docx', 'DOCX', 1, '2021-06-13 05:52:49', '2021-06-13 05:52:49'),
(6, 'a:3:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:23:\"1623565556_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:23:\"1623565556_Archivo1.png\";}i:2;a:6:{s:2:\"id\";i:7;s:14:\"urlRepositorio\";s:13:\"Archivo1.jpeg\";s:9:\"extension\";s:4:\"JPEG\";s:10:\"porcentaje\";d:8.24;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:24:\"1623565557_Archivo1.jpeg\";}}', NULL, NULL, 1, '2021-06-13 06:25:57', '2021-06-13 06:25:57'),
(7, 'a:3:{i:0;a:6:{s:2:\"id\";i:1;s:14:\"urlRepositorio\";s:12:\"Archivo1.jpg\";s:9:\"extension\";s:3:\"JPG\";s:10:\"porcentaje\";i:100;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:23:\"1623565668_Archivo1.jpg\";}i:1;a:6:{s:2:\"id\";i:4;s:14:\"urlRepositorio\";s:12:\"Archivo1.png\";s:9:\"extension\";s:3:\"PNG\";s:10:\"porcentaje\";d:16.31;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:23:\"1623565669_Archivo1.png\";}i:2;a:6:{s:2:\"id\";i:7;s:14:\"urlRepositorio\";s:13:\"Archivo1.jpeg\";s:9:\"extension\";s:4:\"JPEG\";s:10:\"porcentaje\";d:8.24;s:11:\"urlComparar\";s:39:\"/proyectos2/QWWW/version1/MineCraft.jpg\";s:12:\"urlResultado\";s:24:\"1623565669_Archivo1.jpeg\";}}', 'MineCraft', 'JPG', 1, '2021-06-13 06:27:49', '2021-06-13 06:27:49'),
(11, 'a:1:{i:0;a:3:{s:3:\"url\";s:13:\"Archivo1.docx\";s:2:\"id\";i:3;s:10:\"porcentaje\";d:100;}}', 'Archivo1docx', 'DOCX', 1, '2021-06-13 15:58:27', '2021-06-13 15:58:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roleproyecto`
--

CREATE TABLE `roleproyecto` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `edit` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remove` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(2, 'Guest', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(3, 'Reporter', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(4, 'Developer', 'web', '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(5, 'JefeProyecto', 'web', '2021-06-05 04:51:04', '2021-06-05 04:51:04'),
(6, 'Prueba', 'web', '2021-06-05 04:51:18', '2021-06-05 04:51:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 6),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(44, 5),
(45, 1),
(45, 5),
(46, 1),
(46, 5),
(47, 1),
(47, 5),
(48, 1),
(48, 2),
(48, 3),
(48, 4),
(48, 5),
(49, 1),
(49, 3),
(49, 4),
(49, 5),
(50, 1),
(50, 5),
(51, 1),
(51, 4),
(51, 5),
(52, 1),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(53, 1),
(53, 3),
(53, 4),
(53, 5),
(54, 1),
(54, 4),
(54, 5),
(55, 1),
(55, 5),
(56, 1),
(56, 6),
(57, 1),
(58, 1),
(59, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposarchivo`
--

CREATE TABLE `tiposarchivo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tiposarchivo`
--

INSERT INTO `tiposarchivo` (`id`, `nombre`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'IMAGENES', 1, '2021-06-05 04:51:03', '2021-06-05 04:51:03'),
(2, 'DOCUMENTOS', 1, '2021-06-05 04:51:03', '2021-06-05 04:51:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baja` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `bloqueo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `disponible` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'SI',
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navegador` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dir_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `baja`, `bloqueo`, `disponible`, `google_id`, `facebook_id`, `navegador`, `dir_ip`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$3FIVTwUw3BVX5cMvzUMyR.MhA7/1c0zj4Vl6/K1TxuVzOXw7oDUPu', 'NO', 'NO', 'NO', NULL, NULL, 'Google Chrome', 'localhost', NULL, NULL, '2021-06-05 04:51:03', '2021-06-13 13:20:45'),
(2, 'prueba', 'prueba@gmail.com', '$2y$10$N6JfaXP1NvaxUBpi7YmFsOGE0s6a06316EJNP6BXGlxRjigQ4h4oa', 'NO', 'NO', 'SI', NULL, NULL, 'Google Chrome', '127.0.0.1', NULL, NULL, '2021-06-05 04:51:26', '2021-06-12 18:20:17'),
(3, 'prueba2', 'prueba2@gmail.com', '$2y$10$gFVj.ICnJ.icCS6iz.D6TOarT0pdcc2KugSn0dhaKKSayy/ac0uva', 'NO', 'NO', 'NO', NULL, NULL, 'Google Chrome', 'localhost', NULL, NULL, '2021-06-05 04:54:12', '2021-06-13 16:36:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivosproyecto`
--
ALTER TABLE `archivosproyecto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivosproyecto_proyecto_id_foreign` (`proyecto_id`),
  ADD KEY `archivosproyecto_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asignaciones_proyecto_id_foreign` (`proyecto_id`),
  ADD KEY `asignaciones_user_id_foreign` (`user_id`),
  ADD KEY `asignaciones_creadopor_foreign` (`creadopor`);

--
-- Indices de la tabla `bloqueos`
--
ALTER TABLE `bloqueos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bloqueos_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `categoria_proyecto`
--
ALTER TABLE `categoria_proyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `extension`
--
ALTER TABLE `extension`
  ADD PRIMARY KEY (`id`),
  ADD KEY `extension_tiposarchivo_id_foreign` (`tiposarchivo_id`),
  ADD KEY `extension_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `personalizados`
--
ALTER TABLE `personalizados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personalizados_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personas_ciudad_id_foreign` (`ciudad_id`),
  ADD KEY `personas_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proyectos_categoria_proyecto_id_foreign` (`categoria_proyecto_id`),
  ADD KEY `proyectos_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `redaccion`
--
ALTER TABLE `redaccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `redaccion_creadopor_foreign` (`creadopor`);

--
-- Indices de la tabla `repositorio_archivo`
--
ALTER TABLE `repositorio_archivo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `repositorio_archivo_extension_id_foreign` (`extension_id`),
  ADD KEY `repositorio_archivo_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `resultados_comparar`
--
ALTER TABLE `resultados_comparar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resultados_comparar_archivosproyecto_id_foreign` (`archivosproyecto_id`);

--
-- Indices de la tabla `res_comp_proyecto`
--
ALTER TABLE `res_comp_proyecto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `res_comp_proyecto_proyectos_id_foreign` (`proyectos_id`);

--
-- Indices de la tabla `roleproyecto`
--
ALTER TABLE `roleproyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `tiposarchivo`
--
ALTER TABLE `tiposarchivo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tiposarchivo_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivosproyecto`
--
ALTER TABLE `archivosproyecto`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `bloqueos`
--
ALTER TABLE `bloqueos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria_proyecto`
--
ALTER TABLE `categoria_proyecto`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `extension`
--
ALTER TABLE `extension`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `personalizados`
--
ALTER TABLE `personalizados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `redaccion`
--
ALTER TABLE `redaccion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `repositorio_archivo`
--
ALTER TABLE `repositorio_archivo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `resultados_comparar`
--
ALTER TABLE `resultados_comparar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `res_comp_proyecto`
--
ALTER TABLE `res_comp_proyecto`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `roleproyecto`
--
ALTER TABLE `roleproyecto`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tiposarchivo`
--
ALTER TABLE `tiposarchivo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivosproyecto`
--
ALTER TABLE `archivosproyecto`
  ADD CONSTRAINT `archivosproyecto_proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`),
  ADD CONSTRAINT `archivosproyecto_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  ADD CONSTRAINT `asignaciones_creadopor_foreign` FOREIGN KEY (`creadopor`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `asignaciones_proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`),
  ADD CONSTRAINT `asignaciones_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `bloqueos`
--
ALTER TABLE `bloqueos`
  ADD CONSTRAINT `bloqueos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `extension`
--
ALTER TABLE `extension`
  ADD CONSTRAINT `extension_tiposarchivo_id_foreign` FOREIGN KEY (`tiposarchivo_id`) REFERENCES `tiposarchivo` (`id`),
  ADD CONSTRAINT `extension_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `personalizados`
--
ALTER TABLE `personalizados`
  ADD CONSTRAINT `personalizados_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ciudad_id_foreign` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudades` (`id`),
  ADD CONSTRAINT `personas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `proyectos_categoria_proyecto_id_foreign` FOREIGN KEY (`categoria_proyecto_id`) REFERENCES `categoria_proyecto` (`id`),
  ADD CONSTRAINT `proyectos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `redaccion`
--
ALTER TABLE `redaccion`
  ADD CONSTRAINT `redaccion_creadopor_foreign` FOREIGN KEY (`creadopor`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `repositorio_archivo`
--
ALTER TABLE `repositorio_archivo`
  ADD CONSTRAINT `repositorio_archivo_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `extension` (`id`),
  ADD CONSTRAINT `repositorio_archivo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `resultados_comparar`
--
ALTER TABLE `resultados_comparar`
  ADD CONSTRAINT `resultados_comparar_archivosproyecto_id_foreign` FOREIGN KEY (`archivosproyecto_id`) REFERENCES `repositorio_archivo` (`id`);

--
-- Filtros para la tabla `res_comp_proyecto`
--
ALTER TABLE `res_comp_proyecto`
  ADD CONSTRAINT `res_comp_proyecto_proyectos_id_foreign` FOREIGN KEY (`proyectos_id`) REFERENCES `proyectos` (`id`);

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tiposarchivo`
--
ALTER TABLE `tiposarchivo`
  ADD CONSTRAINT `tiposarchivo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
