<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('baja')->default('NO');
            $table->string('bloqueo')->default('NO');
            $table->string('disponible')->default('SI');
            $table->string('google_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('navegador')->nullable();
            $table->string('dir_ip')->nullable();

            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('ci')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudades');

            // $table->unsignedBigInteger('role_id');
            // $table->foreign('role_id')->references('id')->on('roles');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        }); 
    }
 
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
