<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRepositorioArchivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repositorio_archivo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('md5_file');
            $table->string('estado');
            $table->string('ocr')->default('NO');
            $table->string('recorte')->default('NO');
            $table->unsignedBigInteger('extension_id');
            $table->foreign('extension_id')->references('id')->on('extension');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repositorio_archivo');
    }
}
