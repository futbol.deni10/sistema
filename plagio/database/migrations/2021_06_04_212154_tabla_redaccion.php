<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRedaccion extends Migration
{

    public function up()
    {
        Schema::create('redaccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->text('contenido')->nullable();
            $table->text('asignados')->nullable();
            $table->unsignedBigInteger('creadopor');
            $table->foreign('creadopor')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('redaccion');
    }
}
