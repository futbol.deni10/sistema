<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaResultadoComparar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados_comparar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('a_comparados');
            //$table->string('tipo');
            $table->unsignedBigInteger('archivosproyecto_id');
            $table->foreign('archivosproyecto_id')->references('id')->on('repositorio_archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados_comparar');
    }
}
