<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableResCompararProye extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('res_comp_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('a_comparados');
            $table->string('archivo');
            $table->string('extension');
            $table->unsignedBigInteger('proyectos_id');
            $table->foreign('proyectos_id')->references('id')->on('proyectos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('res_comp_proyecto');
    }
}
