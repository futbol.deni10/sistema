<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{

	protected $table = "tiposarchivo";

    protected $fillable = [
        'nombre','user_id'
    ];

}
