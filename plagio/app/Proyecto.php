<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
	protected $table = "proyectos";

    protected $fillable = ['nombre','descripcion','tipo_proyecto','categoria_proyecto_id','user_id','baja'];

}