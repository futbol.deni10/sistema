<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RedaccionEvent implements ShouldBroadcast
{
    public $message;

    public function __construct($mensaje)
    {
        $this->message = $mensaje;
    }


    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');

        return ['redaccionchannel']; 
        //return new Channel('redaccionchannel');
    }

    public function broadcastAs()
    {
        return "redevent";
    }
}
