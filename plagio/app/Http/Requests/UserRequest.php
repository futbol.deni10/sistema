<?php
 
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
            //'role_id' => 'required',
            // 'add' => 'required',
            // 'edit' => 'required',
            // 'remove' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'El campo Usuario es obligatorio',
            'username.unique' => 'Ya Existe el Usuario',
            'email.unique' => 'Ya Existe el Email',
            //'username.min' => 'El campo Usuario necesita minimo 4 caracteres    ',
        ];
    }
}
