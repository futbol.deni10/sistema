<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CategoriaProyecto;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class CategoriaProyectoController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:CategoriaProyecto-list|CategoriaProyecto-create|CategoriaProyecto-edit', ['only' => ['index','store']]);
        $this->middleware('permission:CategoriaProyecto-create', ['only' => ['create','store']]);
        $this->middleware('permission:CategoriaProyecto-edit', ['only' => ['edit','update']]);
    }

    public function index()
    {
        return view('admin.categoriaproyecto.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if($request->ajax())
        {
            $CategoriaProyecto = new CategoriaProyecto($request->all());
            $CategoriaProyecto->save();

            return response()->json($CategoriaProyecto);
        }
    }

    public function listar_Categorias()
    {
        $proyecto =  DB::table('categoria_proyecto')->get();
        if(request()->ajax()){
            return Datatables::of($proyecto)
                ->rawColumns( ['id','nombre','username','created_at'])
                ->make(true);
        }else{
            abort('404');
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        return CategoriaProyecto::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dato = CategoriaProyecto::find($id);
        $dato->nombre = $request->nombre;
        $dato->save();
        return response()->json($dato);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
