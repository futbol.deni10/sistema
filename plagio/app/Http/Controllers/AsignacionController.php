<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Proyecto;
use App\Asignacion;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class AsignacionController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:AsignarUsuario-list|AsignarUsuario-create|AsignarUsuario-edit|AsignarUsuario-delete', ['only' => ['index','store']]);
        $this->middleware('permission:AsignarUsuario-create', ['only' => ['create','store']]);
        $this->middleware('permission:AsignarUsuario-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:AsignarUsuario-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $Proyecto = Proyecto::orderBy('nombre','ASC')->where('baja','NO')->pluck('nombre','id');
        $User = User::orderBy('username','ASC')->pluck('username','id');
        //$Roles = UserProyecto::orderBy('id','ASC')->pluck('name','id');
        $roles = Role::from('roles as r') 
                ->join('role_has_permissions as rp','rp.role_id','r.id')
                ->join('permissions as p','p.id','rp.permission_id')
                ->where('p.name','like','%GestorArchivoProyecto%')
                ->where('r.name','<>','Administrador')
                //->orWhere('p.name','like','%GestionVersiones%')
                ->orderBy('r.id','ASC')
                ->pluck('r.name','r.name')
                ->all();


        // $users = User::orderBy('id','DESC')->get();
        // $users->each(function ($users)
        // {
        //     $users->activo();
        //     $users->getRoleNames();
        // });
        // dd($users);
        return view('admin.asignaciones.index')
                ->with('proyectos',$Proyecto)
                ->with('users',$User)   
                ->with('Roles',$roles); 
    }

    public function listar_asignacion()
    {
        $proyecto =  DB::table('asignaciones as a')
                        ->join('users as u','u.id','=','a.user_id')
                        ->join('users as us','us.id','=','a.creadopor')
                        ->join('proyectos as p','p.id','=','a.proyecto_id')
                        ->select('a.*','p.nombre',
                                'u.username',
                                'us.username as creadopor')
                        ->get();
        if(request()->ajax()){
        return Datatables::of($proyecto)
         ->rawColumns( ['id','nombre','username','creadopor','created_at','tiempo','estado'])
         ->editColumn('created_at', function ($proyecto) {
                $dt = Carbon::create($proyecto->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
         ->editColumn('tiempo', function ($proyecto) {
                if($proyecto->tiempo != null)
                {                    
                    $dt = Carbon::create($proyecto->tiempo)->locale('es');
                    $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                    return $fecha;
                }else
                    return "Ilimitado";
            })
         ->make(true);
        }else{
            abort('404');
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //dd($request->input('roles'));
        if($request->ajax())
        {
            $check = DB::table('asignaciones')
                    ->where('user_id',$request->user_id)
                    ->where('proyecto_id',$request->proyecto_id)->exists();
            $mensaje = '';
            if(!$check)
            { 
                $Asignacion = new Asignacion($request->all());
                $Asignacion->creadopor = Auth::user()->id;
                $Asignacion->user_id = $request->user_id;
                $user = User::find($request->user_id);
                //dd($user->hasRole($request->input('roles')));
                if($user->hasRole($request->input('roles')) == false)
                {
                    $user->assignRole($request->input('roles'));
                    //dd($user->hasRole($request->input('roles')));
                }
            }else {
                $mensaje = 'El usuario ya se encuentra asignado en este proyecto';
            }

            return response()->json(['mensaje' => $mensaje]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $Asignacion = Asignacion::find($id);
        $User = User::find($Asignacion->user_id);
        $User->getRoleNames();
        return response()->json(['asignacion' => $Asignacion,'usuario' => $User]);
    }


    public function update(Request $request, $id)
    {
        $Asignacion = Asignacion::find($id);
        $Asignacion->proyecto_id = $request->proyecto_id;
        $Asignacion->user_id = $request->user_id;
        if($request->tiempo != '')
            $Asignacion->tiempo = $request->tiempo;
        else
            $Asignacion->tiempo = null;
        $Asignacion->roleproyecto_id = $request->roleproyecto_id;
        $Asignacion->save();
        return response()->json($Asignacion);
    }

    public function destroy($id)
    {
        //
    }

    public function actualizarAsignacion(){

        $dato = DB::table('asignaciones')->where('estado','activo')->get();
        $hoy = Carbon::today()->locale('es');
        $check = false;
        for($i=0; $i<sizeof($dato); $i++)
        {
            if($dato[$i]->tiempo != null)
            {

                $fecha_final = Carbon::create($dato[$i]->tiempo);
                if($hoy > $fecha_final)
                {
                    $Asignacion = Asignacion::find($dato[$i]->id);
                    $Asignacion->estado = 'inactivo';
                    $Asignacion->save();
                    $check = true;
                    //$this->actualizarDescuento = "Los descuentos fueron actualizados";
                }
            }
        }

        return response()->json($check);
    }
}
