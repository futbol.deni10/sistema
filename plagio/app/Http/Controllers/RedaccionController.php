<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\Redaccion;
use Brian2694\Toastr\Facades\Toastr;

//use App\Events\NuevaOrdenEvent;
use Pusher\Laravel\Facades\Pusher;
use Illuminate\Support\Facades\Event;
class RedaccionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Redaccion-list|Redaccion-create|Redaccion-edit|Redaccion-delete', ['only' => ['index','store']]);
        $this->middleware('permission:Redaccion-create', ['only' => ['create','store']]);
        $this->middleware('permission:Redaccion-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:Redaccion-delete', ['only' => ['destroy']]);
    }

    public function index(){
        $Users = User::orderBy('username','ASC')
                ->where('username','<>','admin')
                ->pluck('username','id');

        return view('admin.redaccion.index')
                ->with('users',$Users);
    }

    public function indexEditor($criterio)
    {
        
        $u = User::orderBy('username','ASC')
                ->where('username','<>','admin')
                ->pluck('username','id');
        $Redaccion = Redaccion::find($criterio);
        $noredaccion = '';
        if($Redaccion)
        {            
            $users = unserialize($Redaccion->asignados);
            
            $check= in_array(strval(Auth::user()->id), $users);
            //dd($check);
            if($check)
            {   if($Redaccion->creadopor == Auth::user()->id)
                    return view('admin.redaccion.ver')->with('who','CREADOR')->with('criterio',$Redaccion)->with('users',$u);
                else
                    return view('admin.redaccion.ver')->with('who','INVITADO')->with('criterio',$Redaccion)->with('users',$u);
            }else{
                $noredaccion = 'No tienes permiso para participar en la redaccion';
                Toastr::error($noredaccion);
                return redirect()->route('home');
            }
        }else
        {
            $noredaccion = 'La url de redaccion no existe';
            Toastr::error($noredaccion);
            return redirect()->route('home');
        }

    }
    
    public function listar_redacciones()
    {
           
        $check = Auth::user()->hasRole('Administrador');
        if($check)
        {
            $datos =  DB::table('redaccion as r')
            ->join('users as u','u.id','=','r.creadopor')
            ->select('r.*',
                    'u.username')
            ->get();
        }else
        {
            $datos =  DB::table('redaccion as r')
            ->join('users as u','u.id','=','r.creadopor')
            ->select('r.*',
                    'u.username')
            ->where('r.creadopor',Auth::user()->id)
            ->get();
        }


        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','username','nombre','created_at'])
             ->editColumn('created_at', function ($datos) {
                    $dt = Carbon::create($datos->created_at)->locale('es');
                    $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                    return $fecha;
                })
             ->make(true);
            }else{
                abort('404');
        }
    }

    public function create()
    {


    }

    public function store(Request $request)
    {   if($request->user_id){
            $cad = $request->user_id;
            array_push($cad,strval(Auth::user()->id));
        }
        else{
            $cad = [];
            array_push($cad,strval(Auth::user()->id));
        }
        
        if($request->ajax())
        {
            $Redaccion = new Redaccion();
            $Redaccion->nombre = $request->nombre;
            $Redaccion->asignados = serialize($cad);
            $Redaccion->creadopor = Auth::user()->id;
            $Redaccion->save();
            return response()->json($Redaccion);
        }
    }

    public function storeUsuario(Request $request){
        //dd($request->all());
        $Redaccion = Redaccion::find($request->criterio);
        $cad = unserialize($Redaccion->asignados);
        for($i=0; $i<sizeof($request->users); $i++){
            $check = in_array(strval($request->users[$i]), $cad);
            if(!$check)
                array_push($cad,strval($request->users[$i]));

        }
        $Redaccion->asignados = serialize($cad);
        $Redaccion->save();
        return response()->json($Redaccion);
    }

    public function guardarTexto (Request $request){
        $Redaccion = Redaccion::find($request->id);
        $Redaccion->contenido = $request->contenido;
        $Redaccion->save();
        //event(new \App\Events\NuevaOrdenEvent($request->contenido));
        return response()->json($Redaccion);
    }

    public function getRedaccion($id){
        return Redaccion::find($id);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function eliminarRedaccion($criterio)
    {
        try {
            $datos = Redaccion::find($criterio);
            $datos->delete();
            return response()->json(['mensaje'=>'']);
        } catch(\Exception $exception){
            return response()->json(['mensaje'=>'Registro en Uso no puede ser eliminado']);
        }
    }
 
    public function destroy($id)
    {
        //
    }
}
