<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proyecto;
use App\Asignacion;
use App\User;
//use App\Role;
use App\ArchivosProyecto;
use App\CategoriaProyecto;
use ZipArchive;
use App\UserProyecto;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;
class ProyectoController extends Controller
{
    

    public function index()
    {
        $CategoriaProyecto = CategoriaProyecto::pluck('nombre','id');
        return view('admin.proyectos.index')
                ->with('CategoriaProyecto',$CategoriaProyecto); 
    }

    public function listar_proyectos()
    {
        $proyecto =  DB::table('proyectos as p')
        ->join('users as u','u.id','=','p.user_id')
        ->select('p.*',
                    'u.username')
        ->get();
        if(request()->ajax()){
        return Datatables::of($proyecto)
         ->rawColumns( ['id','nombre','username','created_at'])
         ->editColumn('created_at', function ($proyecto) {
                $dt = Carbon::create($proyecto->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
         ->make(true);
        }else{
            abort('404');
        }
    }



    public function listar_archivosP($criterio){
        $user = Auth::user();
        if($user->role->name == 'Administrador')
        {
            $check = DB::table('asignaciones')
                ->where('proyecto_id',$criterio)
                //->where('user_id',$user->id)
                ->exists();
        }else {

            $check = DB::table('asignaciones')
                ->where('proyecto_id',$criterio)
                ->where('user_id',$user->id)
                ->exists();
        }
        if($check)
        {            
            $archivo = DB::table('archivosproyecto as ap')
                ->join('users as u','u.id','=','ap.user_id')
                ->select('ap.*',
                            'u.username')
                ->where('ap.proyecto_id',$criterio)
                ->get();
        }else{
            $archivo = DB::table('archivosproyecto as ap')
                ->join('users as u','u.id','=','ap.user_id')
                ->select('ap.*',
                            'u.username')
                ->where('ap.proyecto_id',0)
                ->get(); 
        }

        if(request()->ajax()){
        return Datatables::of($archivo)
         ->rawColumns( ['id','nombre','username','created_at'])
         ->editColumn('created_at', function ($archivo) {
                $dt = Carbon::create($archivo->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
         ->make(true);
        }else{
            abort('404');
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if($request->ajax())
        {
            $Proyecto = new Proyecto($request->all());
            $Proyecto->user_id = Auth::user()->id;
            $Proyecto->save();
            
            $Asignacion = new Asignacion();
            $Asignacion->proyecto_id = $Proyecto->id;
            $Asignacion->user_id = Auth::user()->id;
            $Asignacion->creadopor = Auth::user()->id;
            $Asignacion->save();

            if(Auth::user()->hasRole('JefeProyecto') == false)
            {
                Auth::user()->assignRole('JefeProyecto');
                //dd($user->hasRole($request->input('roles')));
            }

            return response()->json($Proyecto);
        }
    }
  
    public function estadoProyecto(Request $request)
    {

        if($request->criterio == '1')
        {
            $Proyecto = Proyecto::find($request->id);
            $Proyecto->baja = "SI";
            $Proyecto->save();
            $mensaje = "El proyecto fue dado de baja";
        }else{
            $Proyecto = Proyecto::find($request->id);
            $Proyecto->baja = "NO";
            $Proyecto->save();
            $mensaje = "El proyecto fue activado";
        }

        return response()->json($mensaje);
    }

    public function getArchivoProyecto($criterio)
    {
        $user = Auth::user();
        $datos = ArchivosProyecto::find($criterio);
        $proyecto = Proyecto::find($datos->proyecto_id);
        $route = public_path() .'/proyectos2/'.$proyecto->nombre.'/'.$datos->archivo;
        if($user->role->name == 'Administrador')
        {           
            $headers = array(
                      'Content-Type: application/pdf',
                    );
            
            return response()->download($route, $datos->archivo, [], 'inline');
        }else{

            //$check = DB::table('archivosproyecto')->where('user_id',$user->id)->exists();
            $check = DB::table('asignaciones')
                    ->where('user_id',$user->id)
                    ->where('proyecto_id',$proyecto->id)->exists();
            if($check)
            {
                $headers = array(
                          'Content-Type: application/pdf',
                        );
                
                $nameFile = $datos->url;
                return response()->download($route, $datos->archivo, [], 'inline');
            }else {
                $nopermiso = "El archivo no te pertenece, contáctese con el administrador";
                return redirect()->route('home')->with(compact('nopermiso'));
            }
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $Proyecto = Proyecto::find($id);
        return $Proyecto;
    }


    public function update(Request $request, $id)
    {
        $Proyecto = Proyecto::find($id);
        $Proyecto->nombre = $request->nombre;
        $Proyecto->descripcion  = $request->descripcion;
        $Proyecto->tipo_proyecto = $request->tipo_proyecto;
        $Proyecto->categoria_proyecto_id  = $request->categoria_proyecto_id ;
        $Proyecto->save();

        return response()->json($Proyecto);
    }

    public function updateAsignacion(Request $request, $id)
    {
        $Asignacion = Asignacion::find($id);
        $Asignacion->proyecto_id = $request->proyecto_id;
        $Asignacion->user_id = $request->user_id;
        $Asignacion->tiempo = $request->tiempo;
        $Asignacion->roleproyecto_id = $request->roleproyecto_id;
        $Asignacion->save();
        return response()->json($Asignacion);
    }

    public function destroy($id)
    {
        //
    }



}
