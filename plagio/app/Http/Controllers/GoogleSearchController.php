<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AlesZatloukal\GoogleSearchApi\GoogleSearchApi;
use DataTables;

class GoogleSearchController extends Controller
{

    public function index()
    {
        return view('admin.googlesearch.index');
    } 

    public function gerResultados(Request $request)
    {
        //https://www.googleapis.com/customsearch/v1?key=AIzaSyAX1Eubq7LIbAAZ4SfUhmiYclxaRnBE1BU&cx=3864ba8520f9aa8ac&q=Guitarra
        $googleSearch = new GoogleSearchApi();
        $parametros = array(
            'start' => 1, // start from the 10th results,
            'num' => 5 // number of results to get, 10 is maximum and also default value
        );
        $results = $googleSearch->getResults($request->contenido);
        //$results = $googleSearch->getResults($request->contenido,$parametros);
        //dd($results);
        //$rawResults = $googleSearch->getRawResults();
        //return response()->json(['resultados'=>$results]);
        if(request()->ajax()){
            return Datatables::of($results)
            ->rawColumns( ['id','link','titulo','htmlSnippet'])
            ->make(true);
        }else{
            abort('404');
        }
    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
