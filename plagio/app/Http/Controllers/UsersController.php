<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserEditRequest;
use App\User;
use App\Ciudad;
use App\Personalizado;

//use Laracast\flash\flash;
//use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use DataTables;
//use Validator; 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersController extends Controller
{ 
    
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function index()
    {  

        //dd(DB::table('model_has_roles')->where('model_id',Auth::user()->id));
        $users = User::orderBy('id','ASC')->get();
        //$roles  = Role::orderBy('name','ASC')->pluck('name','id');
        // $roles = Role::orderBy('id','ASC')->get();
        
        // $roles->each(function ($roles)
        // {
        //     $roles->permission;
        //     $roles->users;

        // });
 
        // $user = Auth::user();
        // dd(Auth::user()->role->permission);
        //$roles = Role::pluck('name','name')->all();
        $roles = Role::from('roles as r')
                ->join('role_has_permissions as rp','rp.role_id','r.id')
                ->join('permissions as p','p.id','rp.permission_id')
                ->where('p.name','<>','GestorArchivoProyecto-list')
                ->where('p.name','<>','GestorArchivoProyecto-edit')
                ->where('p.name','<>','GestorArchivoProyecto-create')
                ->where('p.name','<>','GestorArchivoProyecto-delete')
                ->where('p.name','<>','GestionVersiones-list')
                ->where('p.name','<>','GestionVersiones-edit')
                ->where('p.name','<>','GestionVersiones-create')
                ->where('p.name','<>','GestionVersiones-delete')
                ->where('p.name','<>','AsignarUsuario-list')
                ->where('p.name','<>','AsignarUsuario-edit')
                ->where('p.name','<>','AsignarUsuario-create')
                ->where('p.name','<>','AsignarUsuario-delete')
                ->where('r.name','<>','Administrador')
                //->orWhere('p.name','like','%GestionVersiones%')
                ->orderBy('r.id','ASC')
                ->pluck('r.name','r.name')
                ->all();
        $ciudades = Ciudad::orderBy('id','ASC')->pluck('nombre','id');
        return view('admin.users.index') 
        ->with('users',$users)
        ->with('ciudades',$ciudades)
        ->with('roles',$roles);
    }

    public function list_user(){

        // $users = User::orderBy('id','ASC')
        //         ->join('roles','users.role_id','=','roles.id')
        //         ->select('users.*','roles.name as rol_nombre')
        //         ->get();
        $users = User::orderBy('id','DESC')->get();
        $users->each(function ($users)
        {
            $users->activo();
            $users->getRoleNames();
        });
        //dd($users);
        if(request()->ajax()){
            return Datatables::of($users)
             ->rawColumns(['id','username','name','baja'])
             ->editColumn('baja', function ($users) {
                    if ($users->baja == 'NO')
                        $m = 'Activo';
                    if ($users->baja == 'SI')
                        $m = 'Inactivo';
                    return $m;
            }) 
            ->editColumn('remember_token', function ($users) {
                    if($users->activo())
                        $m ="ONLINE";
                    else
                        $m = "OFFLINE";
                    return $m;
            })
             ->make(true);
            }else{
                abort('404');
        }

    }
    public function store(UserRequest $request)
    { 
        //dd($request->input('roles'));
        $user = new User($request->all());


        $user->password = bcrypt($request->password);
        $user->save();
        $user->assignRole($request->input('roles'));

        //Toastr::success('Usuario '. $user[0]->name.' registrado','');
        //return redirect()->route('users.index');
        return response()->json($user);
       // }
       // return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        $user = User::find($id);
       // $user->getAuthPassword();
        //$user->role;
        $user->permission;
        return response()->json($user);     
    }

    public function edit($id)
    {
        $user = User::find($id);
       // $user->getAuthPassword();
        //$user->role;
        //$user->permission;
        $user->roles->pluck('name','name')->all();
        return response()->json($user);
    }
    public function update(UserEditRequest $request, $id)
    {
        if ($request->ajax())
        {
            $mensaje = "";
            $a = DB::table('users')->where('username',$request->username)->count();
            $usuario = DB::table('users')->where([
                    ['id', '=', $id],
                    ['username', '=', $request->username]
                ])->count();
            $contador = 0;
            if ($a - $usuario == 1)
            {
                $contador = $contador + 1;
                $mensaje = $mensaje . "El Usuario ya Existe." . "<br/>" ;
            }
            $b = DB::table('users')->where('email',$request->email)->count();
            $email = DB::table('users')->where([
                    ['id', '=', $id],
                    ['email', '=', $request->email]
                ])->count();
            if ($b - $email == 1)
            {
                $contador = $contador + 2;
                $mensaje = $mensaje . "El Email ya Existe." . "\n" ;
            }
            if($mensaje == "" )
            {
                //dd($request->all());
                $input = $request->all();
                $user = User::find($id);
                $user->update($input);
                // $user->username = $request->username;
                // $user->email = $request->email;
                // $user->nombre = $request->nombre;
                // $user->ci = $request->ci;
                // $user->ciudad_id = $request->ciudad_id;
                // $user->telefono = $request->telefono;
                // $user->direccion = $request->direccion;

                DB::table('model_has_roles')->where('model_id',$id)->delete();
                $user->assignRole($request->input('roles'));
                //$user->save();
                // $user = User::FindOrFail($id);
                // //$user->role_id = $request->role_id;
                // $user->username = $request->username;
                // $user->email = $request->email;
                // //$user->password = bcrypt($request->password); 
                // $user->save();
                $mensaje='Usuario '. $user->username.' Fue Editado'; 
                //Toastr::success('Usuario '. $user->username.' Fue Editado',''); 
                return response()->json(['success'=>'true','mensaje'=>$mensaje]);             
            }else
            {
                return response()->json(['error'=>'true','cont'=>$contador,'mensaje'=>$mensaje]);
            }    
        }
    }

    public function bajaUsuario(Request $request)
    {
        $User = User::findOrFail($request->id);
        $User->baja = "SI";
        $User->save();
        return response()->json([$User]); 
    }
    public function activarUsuario(Request $request)
    {
        $User = User::findOrFail($request->id);
        $User->baja = "NO";
        $User->save();
        return response()->json([$User]); 
    }

    public function destroy(Request $request)
    {

    }

    public function addRole($criterio,$role)
    {
        $user = User::find($criterio);
        $user->assignRole($role);
        return response()->json('OK');
    }

    public function storePersonalizar(Request $request)
    {

        if($request->ajax()){
            $check = DB::table('personalizados')->where('user_id',Auth::user()->id)->exists();
            if(!$check){                
                $personalizado = new Personalizado($request->all());
                $personalizado->user_id = Auth::user()->id;
                $personalizado->vertical_effect = 'shrink';
                if($request->file != "undefined")
                {
                    $file = $request->file;
                    $userName = trim(str_replace(' ', '', auth()->user()->username));
                    $nameFile = trim($userName) .'_'.time().'.'.$file->getClientOriginalExtension();
                    $path = public_path() . '/personalizado/user/'.Auth::user()->id.'/';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file->move($path,$nameFile);
                    $personalizado->imagen = $nameFile;              
                }
                $personalizado->save();
            }else 
            {
                $dato = DB::table('personalizados')->where('user_id',Auth::user()->id)->get();
                $personalizado = Personalizado::find($dato[0]->id);
                if($request->pcoded_navbar != 'null')
                    $personalizado->pcoded_navbar = $request->pcoded_navbar; 
                if($request->navbar_logo != 'null')
                    $personalizado->navbar_logo = $request->navbar_logo;
                if($request->pcoded_header != 'null')
                    $personalizado->pcoded_header = $request->pcoded_header;
                if($request->active_item_theme != 'null')
                    $personalizado->active_item_theme = $request->active_item_theme;
                if($request->pcoded_navigatio_lavel != 'null')
                    $personalizado->pcoded_navigatio_lavel = $request->pcoded_navigatio_lavel;
                //if($request->nav_type != 'null' && $request->nav_type != 'st2')
                if($request->nav_type != 'null')
                    $personalizado->nav_type = $request->nav_type;
                //if($request->vertical_effect != 'null')
                    $personalizado->vertical_effect = 'shrink';
                if($request->item_border_style != 'null')
                    $personalizado->item_border_style = $request->item_border_style;
                if($request->dropdown_icon != 'null')
                    $personalizado->dropdown_icon = $request->dropdown_icon;
                if($request->subitem_icon != 'null')
                    $personalizado->subitem_icon = $request->subitem_icon;
                
                if($request->file != "undefined")
                {
                    $file = $request->file;
                    $userName = trim(str_replace(' ', '', auth()->user()->username));
                    $nameFile = trim($userName) .'_'.time().'.'.$file->getClientOriginalExtension();
                    $path = public_path() . '/personalizado/user/'.Auth::user()->id.'/';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file->move($path,$nameFile);
                    $personalizado->imagen = $nameFile;              
                }
                
                $personalizado->save();

            }
            return response()->json('ok');
        }
    }

    public function getPersonalizar()
    {
        $user = Auth::user();
        $dato = DB::table('personalizados')->where('user_id',$user->id)->get();
        return response()->json($dato);
    }

    public function profile()
    {
        $user = Auth::user();
        $user->roles->pluck('name','name')->all();
        //$user->persona;
        //dd($user);
        return view('admin.users.profile')->with('user',$user);
    }

    public function actualizardatos(Request $request , $id)
    {
        $User = User::findOrFail($id);
        $User->nombre = $request->nombre;
        $User->apellido = $request->apellido;
        $User->ciudad_id = $request->ciudad_id;
        $User->ci = $request->ci;
        $User->telefono = $request->telefono;
        $User->direccion = $request->direccion;
        $User->save();

        return response()->json('ok');
    }

}
