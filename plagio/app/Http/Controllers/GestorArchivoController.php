<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proyecto;
use App\ArchivosProyecto;
use App\Asignacion;
use App\User;
use App\ResComProyecto;

use ZipArchive;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Jobs\ProcessArchivo;
use App\Jobs\ProcessCompararProyecto;

use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
class GestorArchivoController extends Controller
{

    function __construct()
    {
        // $this->middleware('permission:GestorArchivoProyecto-list|GestorArchivoProyecto-create|GestorArchivoProyecto-edit|GestorArchivoProyecto-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:GestorArchivoProyecto-create', ['only' => ['create','store']]);
        // $this->middleware('permission:GestorArchivoProyecto-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:GestorArchivoProyecto-delete', ['only' => ['destroy']]);
        //\Queue::connection('database');
    }
    
    public function index()
    {
        $user = Auth::user();
        //$role = Role::where('user_id',$user->id)->get();
        //dd($role);
        // if($user->role->name == 'Administrador')
        // {
            $datos = Asignacion::from('asignaciones as a')
                    ->join('proyectos as p','p.id','a.proyecto_id')
                    //->join('archivosproyecto as ar','ar.proyecto_id','p.id')
                    ->where('p.baja','NO')
                    //->select('a.id')
                    //->select('p.nombre','p.id')->get();
                    ->pluck('p.nombre','p.id');
        // }else {
        //     $datos = Asignacion::from('asignaciones as a')
        //             ->join('proyectos as p','p.id','a.proyecto_id')                    
        //             ->where('a.user_id',$user->id)
        //             ->pluck('p.nombre','p.id');
        // } 
        //dd($datos);
        return view('admin.archivosproyecto.index')
                ->with('datos',$datos);
    }

    public function resultadoproyecto()
    {
        $porcentaje = [''=>'',
                     '0'=>'0%',
                    '5'=>'5%',
                    '10'=>'10%',
                    '20'=>'20%',
                    '30'=>'30%',
                    '40'=>'40%',
                    '50'=>'50%',
                    '60'=>'60%',
                    '70'=>'70%',
                    '80'=>'80%',
                    '90'=>'90%',
                    '100'=>'100%'];
        $cantidad = [''=>'',
                    '5'=>5,
                    '10'=>10,
                    '20'=>20];
        return view('admin.archivosproyecto.ver_index')
                ->with('cantidad',$cantidad)
                ->with('porcentaje',$porcentaje);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    { 
        //dd($request->all());
        $archivo = $request->archivo;
        $destinationPath = public_path();
        $ex = $archivo->getClientOriginalExtension();
        $nombre = $archivo->getClientOriginalName();
        if($ex != 'zip')
        {
            $proyecto = Proyecto::find($request->proyecto_id);

            if($request->dirHere != ''){
                $directorio = str_replace("-","/",$request->dirHere,$i);
                $cadVec = explode("/", $directorio);
                
                $contadores = array_count_values($cadVec);
                $cantidad = array_key_exists('..',$contadores)?$contadores['..']:0;
                if($cantidad != 0)
                {                    
                    while ($cantidad+1 != 0) {
                        $le = sizeof($cadVec);
                        unset($cadVec[$le-1]);
                        $cantidad--;
                    }
                }
                $ruta = implode("/", $cadVec);
                $path = public_path() .'/proyectos2/'. $ruta.'/';
            }
            else
            {
                $path = public_path() .'/proyectos2/'.$proyecto->nombre.'/'.$request->version.'/';

            }
            
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $archivo->move($path, $nombre);
            
            if($request->verificar == 'true')
            {
                $mpdf = new \Mpdf\Mpdf();
                $porciones = explode(",", $request->criterio);
                foreach ($porciones as $key => $c) {            
                    $mpdf->WriteHTML($c);
                }
                //$nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/'.$request->version.'/';
                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                $pos = strpos( $nombre,'.');
                $urlDoc = substr($nombre,0,$pos) . '.pdf';
                
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/'.$request->version.'/'.$urlDoc;
                $mpdf->Output($route,'F');
            }
            if($ex == 'xlsx')
            {
                $cadena = '';
                $spreadsheet = IOFactory::load($request->archivo);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                //dd($sheetData);
                $indice = 65;
                $mayor = 0; 
                for($i=1; $i<=count($sheetData)-1; $i++)
                {
                    if($mayor < count($sheetData[$i]))
                        $mayor = count($sheetData[$i]);
                }
                //echo $mayor;
                for($i=1; $i<=count($sheetData); $i++)
                {
                    for($j=0; $j<$mayor; $j++)
                    {
                        if($sheetData[$i][chr($indice)] != null)
                        {
                            $cadena .= '<p>'. $sheetData[$i][chr($indice)] .'</p><br> ,' ;
                            $indice++;
                        }
                    }
                    $indice = 65;
                }
 
                $mpdf = new \Mpdf\Mpdf();
                $porciones = explode(",", $cadena);
                foreach ($porciones as $key => $c) {            
                    $mpdf->WriteHTML($c);
                }
                $nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/'.$request->version.'/';
                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/'.$request->version.'/'.$nombre;
                $mpdf->Output($route,'F');
            }

            $proyecto = Proyecto::find($request->proyecto_id);
            $version = DB::table('archivosproyecto')->where('archivo',$proyecto->nombre)->exists();
            if(!$version)
            {                
                $dato = new ArchivosProyecto();
                //$dato->archivo = $fileinfo['basename'];
                $dato->archivo = $proyecto->nombre;
                $dato->proyecto_id = $request->proyecto_id;
                $dato->user_id = Auth::user()->id;;
                $dato->version = 1;
                $dato->save();
            }
        }else
        {
            $archivo->move($destinationPath, $nombre);

            ProcessArchivo::dispatch($request->all(), Auth::user()->id,$nombre);
        }
        return response()->json('ok');
    }

    public function listar_resultados2()
    {
                $datos = DB::table('res_comp_proyecto as rp')
                ->join('proyectos as p','p.id','rp.proyectos_id')
                ->join('asignaciones as a','a.proyecto_id','p.id')
                ->select(

                        'rp.created_at',
                        'rp.id','rp.archivo as nombrearchivo')
                ->where('a.user_id',Auth::user()->id)
                ->get();

        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario'])
                         ->editColumn('created_at', function ($datos) {
                $dt = Carbon::create($datos->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
             ->make(true);
        }else{
            abort('404');
        }
    }


    protected $html = '';
    public function listadoDirectorio($directorio,$criterio, $ini, $original){
        //$html = '';
        if($criterio == 0)
        {
            $contenido = array();
                $contenido [] = [
                            'tipo' => '',
                            'dir'=> '' ,
                            'directorio'=> '',
                            'fecha'=>'',
                            'usuario'=>'',
                            'criterio'=>'',
                            'mensaje'=>'SELECCIONE UN PROYECTO',
                            'tipoarchivo' => ''];
                if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                } 
        } 
        //dd($criterio);

        // if(strpos($directorio, "-..-..-..") !== false)
        //     {$ini = 1; dd(str_word_count($directorio));}
        $dato = DB::table('archivosproyecto as a')
                ->join('proyectos as p','p.id','a.proyecto_id')
                ->select('p.tipo_proyecto','p.id','a.*')
                ->where('a.proyecto_id',$criterio)
                ->get();
        //dd($dato);
        $directorio = str_replace("-","/",$directorio,$i);
        $original = str_replace("-","/",$original,$i);
        //$directorio .= '/version1';
        $ruta = public_path() .'/proyectos2/'.$directorio;
       
        $mensaje = '';
        if(file_exists($ruta) && sizeof($dato)>0)
        {
            $check = DB::table('asignaciones')
                ->where('proyecto_id',$dato[0]->proyecto_id)
                ->where('user_id',Auth::user()->id)
                ->where('estado','activo')
                ->exists();
            //dd($check);
            $listado = scandir($ruta);
            //dd($listado);
        }else{
            $contenido = array();
                $contenido [] = [
                            'tipo' => '',
                            'dir'=> '' ,
                            'directorio'=> '',
                            'fecha'=>'',
                            'usuario'=>'',
                            'criterio'=>'',
                            'mensaje'=>'NO EXISTEN ARCHIVOS EN EL PROYECTO SELECCIONADO',
                            'tipoarchivo' => ''];
                if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                }
        }

        if(in_array($original, $listado))
        {
        	//dd("ok");
            $ruta = public_path() .'/proyectos2/'.$dato[0]->archivo.'/'.$original;
            $listado = scandir($ruta);
            $ini = 1;
            $directorio = $dato[0]->archivo.'/'.$original;

        }
        unset($listado[array_search('.', $listado, true)]);
        //unset($listado[array_search('..', $listado, true)]);
        if (count($listado) < 1) {
            $contenido = array();
                $contenido [] = [
                            'tipo' => '',
                            'dir'=> '' ,
                            'directorio'=> '',
                            'fecha'=>'',
                            'usuario'=>'',
                            'criterio'=>'',
                            'mensaje'=>'NO EXISTEN ARCHIVOS EN EL PROYECTO SELECCIONADO',
                            'tipoarchivo' => ''];
                if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                }
        }
        $contenido = array();
        $user = User::find($dato[0]->user_id);
        $dt = Carbon::create($dato[0]->created_at)->locale('es');
        $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
        foreach($listado as $elemento){
            if(is_file ($ruta.'/'.$elemento)) {
                $contenido [] = [
                                'tipo' => 'file',
                                'dir'=> $elemento ,
                                'directorio'=>$directorio.'/'.$elemento ,
                                'fecha'=>$fecha,
                                'usuario'=>$user->username,
                                'criterio'=>$criterio,
                                'mensaje'=>'Proyecto '.$dato[0]->tipo_proyecto,
                                'tipoarchivo'=>'Archivo'
                                ];
            }
            if(is_dir($ruta.'/'.$elemento)) {
                
                if( $ini != 1){

                    $contenido [] = [
                                    'tipo' => 'dir',
                                    'dir'=> $elemento ,
                                    'directorio'=>$directorio.'/'.$elemento,
                                    'fecha'=>$fecha,
                                    'usuario'=>$user->username,
                                    'criterio'=>$criterio,
                                    'mensaje'=>'Proyecto '.$dato[0]->tipo_proyecto,
                                    'tipoarchivo' =>'Directorio'
                                    ];
                }
                if($ini == 1)
                    $ini++;
            }
        }
        if( ($dato[0]->tipo_proyecto == 'Privado'))
        {                  
            $mensaje = 'Proyecto '.$dato[0]->tipo_proyecto;
            if($check || Auth::user()->hasRole('Administrador'))
            {
                if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                }
            }else
            {
                $contenido = array();
                $contenido [] = [
                            'tipo' => '',
                            'dir'=> '/' ,
                            'directorio'=> '',
                            'fecha'=>'',
                            'usuario'=>'',
                            'criterio'=>'',
                            'mensaje'=>'PROYECTO PRIVADO',
                            'tipoarchivo' => ''];
                if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                }
            }
            //$this->html .= '</ul></div>';
        }else{
            if(request()->ajax()){
                    return Datatables::of($contenido)
                    ->rawColumns( ['id','directorio','fecha','usuario']) 
                    ->make(true);
                }else{
                    abort('404');
                }
        }
        

    }

    public function leerDocumento2(Request $request){
        //dd($request->all());
        for($i=0; $i<sizeof($request->datosComparar); $i++){
            $cadVec = explode("-", $request->datosComparar[$i]);
            //dd($cadVec);
            $cad = explode('.',$cadVec[0]);
            $datos = ['nombre'=> $cad[0], 'extension'=>$cad[1],'version'=>$cadVec[2],'url'=>$cadVec[1],'nombreproyecto'=>$cadVec[3],'user_id'=>Auth::user()->id];
            //dd($datos);
            ProcessCompararProyecto::dispatch($datos);
        }
        return response()->json('ok');
    }

    public function getResultados2(Request $request)
    {
            $datos = ResComProyecto::find($request->id);
            $ext = $datos->extension;
            //dd($datos);
            $tipo = DB::table('repositorio_archivo as ra')
                    ->join('extension as e','e.id','ra.extension_id')
                    ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                    ->select('ta.nombre','ra.id','ra.ocr')
                    ->where('e.extension',$ext)->get();

            //$v = DB::table('')

            // if($tipo[0]->ocr == 'SI'){
            //     $id = $tipo[0]->id;
            //     $tipo = [];
            //     $tipo [] = ['nombre'=>'DOCUMENTOS','id'=>$id];
            // }
            
            return response()->json(['unoPor' => unserialize($datos->a_comparados),
                                    'tipo'=>$tipo[0],
                                    'cantidad'=>$request->cantidad,
                                    'porcentaje'=>$request->porcentaje]);
        
    }

    public function getDocumento($criterio,$name){
        //dd($criterio,$name);
        $criterio = str_replace("-","/",$criterio,$i);
        $path = public_path() . '/proyectos2/'. $criterio;
        $headers = array(
                  'Content-Type: application/pdf',
                );
        
        $nameFile = $name;

        return response()->download($path, $nameFile, [], 'inline');
    }

    public function eliminarArchivo($dir,$dirhere){

        $criterio = str_replace("-","/",$dirhere,$i);
        $path = public_path() . '/proyectos2/'. $criterio.'/'.$dir;
        //dd($path);
        if (is_dir($path)) {
            $path = str_replace("\\","/",$path,$i);
            $this->removeDirectory($path);
        }
        if(is_file($path)){
            unlink($path);
        }
        return response()->json('ELIMINADO');
    }

    function deleteFile($path){
        return is_file($path)
        ? unlink($path)
        : array_map('$this->deleteFile', glob($path.'/*')) == $this->deleteFile($path);
    }
       

    function removeDirectory($path)
    {
        $path = rtrim( strval( $path ), '/' ) ;     
        $d = dir( $path );     
        if( ! $d )
            return false;     
        while ( false !== ($current = $d->read()) )
        {
            if( $current === '.' || $current === '..')
                continue;
            $file = $d->path . '/' . $current;     
            if( is_dir($file) )
                $this->removeDirectory($file);     
            if( is_file($file) )
                unlink($file);
        }
        rmdir( $d->path );
        $d->close();
        return true;
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
