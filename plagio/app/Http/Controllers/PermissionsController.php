<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
//use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;
class PermissionsController extends Controller
{

    public function edit($id)
    {
        try{

            $permission = Permission::where('role_id', $id)->firstOrFail();
            $Role = Role::where('id', $id)->firstOrFail();
            $permission->role_name = $Role->name;
            return response()->json($permission);
        }
        catch(\Exception $exception){
            //dd("ok");
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            
            $permission= Permission::where('role_id', $id)->firstOrFail();
            $permission->role_id = $id;
                if($request->add == "true")
                {
                    $permission->add="1";
                }else{
                    $permission->add="0";
                }
                if($request->edit == "true")
                {
                    $permission->edit="1";
                }else{
                    $permission->edit="0";
                }
                if($request->remove == "true")
                {
                    $permission->remove="1";
                }else{
                    $permission->remove="0";
                }
                $permission->save();
                $role=Role::find($id);
        
            //Toastr::success('Permisos del usuario '. $user->name.' editado','');
            return response()->json($role);  
        }
        

    }

}
