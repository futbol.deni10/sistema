<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;
use Carbon\Carbon;
class UsuarioActivo
{

    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $expira = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-' . Auth::user()->id, true, $expira);
        }
        return $next($request);
    }
}
