<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepositorioArchivo extends Model
{
	protected $table = "repositorio_archivo";

    protected $fillable = [
        'url','md5_file','estado','extension_id','user_id','recorte','ocr'
    ];
}
