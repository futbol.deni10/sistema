<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redaccion extends Model
{
    protected $table = "redaccion";

    protected $fillable = ['contenido',
'asignados',
'creadopor'];
}
