<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\ResultadoComparar;
use Illuminate\Support\Facades\Auth;

use Smalot\PdfParser\Parser;

use App\User;

use Illuminate\Support\Facades\DB;
use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Storage;
use App\Jobs\SendEmailJob;
use OCR;
class ProcessCompararOcr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datosRequest;
    public function __construct($request)
    {

        $this->datosRequest = $request;
    }

    public function handle()
    {
        $unoPor = array();
        $resImagenes = array();
        $comparar = DB::table('repositorio_archivo as r')
                    ->join('extension as e','e.id','r.extension_id')
                    ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                    ->select('r.*','e.extension',
                            'ta.nombre as tipo_archivo','ta.id as tiposarchivo_id')
                    ->where('r.id',$this->datosRequest['id'])
                    ->get();

        $ext = DB::table('extension')
                ->where('estado','activo')
                ->where('tiposarchivo_id',2)
                ->get(); 
        if($comparar[0]->recorte == 'NO')
            $urlComparar = public_path() . '/archivocomparar/OCR/'.$comparar[0]->url;
        else
            $urlComparar = public_path() . '/archivocomparar/OCR/RECORTE/Recorte-'.$comparar[0]->url;
        for ($i=0; $i<sizeof($ext); $i++) {
            $idExt = $ext[$i]->id;
            $repositorio = DB::table('repositorio_archivo as ra')
                            ->join('extension as e','e.id','ra.extension_id')
                            ->select('ra.*','e.extension')
                            ->where('ra.estado','archivo')
                            ->where('ra.extension_id',$idExt)
                            ->get();
            //dd($repositorio);
            for($j=0; $j<count($repositorio); $j++)
            {   
                //dd($repositorio[$j]->extension_id);
                if($repositorio[$j]->md5_file == $comparar[0]->md5_file){
                    $resultado [] = ['url'=> $urlDoc2,'id' => $comparar[0]->id];
                }

                $tipo = DB::table('extension as e')
                        ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                        ->where('e.id',$repositorio[$j]->extension_id)
                        ->get();
                //dd($tipo);
                if($repositorio[$j]->extension != 'PDF')
                {
                    $pos = strpos( $repositorio[$j]->url,'.');
                    $urlDoc = substr($repositorio[$j]->url,0,$pos) . '.pdf';

                    $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/convertidos/'.$tipo[0]->extension.'/'. $urlDoc;

                }else {
                    $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'.$tipo[0]->extension.'/'. $repositorio[$j]->url;
                }
                //dd($path);
                $PDFParser = new Parser();
                $pdf = $PDFParser->parseFile($path);
                $pages  = $pdf->getPages();

                $cad1 = "";
                for($p1=0; $p1<sizeof($pages); $p1++){
                    $cad1 .= implode($pages[$p1]->getTextArray());
                }

                $cad1 = implode(" ",array_unique(explode(" ", $cad1)));

                 
                $cad2 =  OCR::scan($urlComparar);

                $cad2 = implode(" ",array_unique(explode(" ", $cad2)));
                similar_text($cad1, $cad2, $porcentaje);

                $unoPor [] = ['url'=> $repositorio[$j]->url,'id' => $repositorio[$j]->id,'porcentaje'=>$porcentaje];          
            }
        }
        $ResultadoComparar = new ResultadoComparar();
        $ResultadoComparar->a_comparados =  serialize($unoPor);
        $ResultadoComparar->archivosproyecto_id = $this->datosRequest['id'];
        $ResultadoComparar->save();

        $idusuario = $comparar[0]->user_id;
        $user = User::find($idusuario);
        
        $datos = ['email'=>$user->email,'mensaje'=>'La comparacion de la imagen fue finalizada <br> Esperamos que los resultados sean lo que tu esperabas, recuerda que la ultima
palabra la tienes tu','url'=>\URL::to('/').'/resultadoIndex','user'=>$user];
        SendEmailJob::dispatch($datos);
    }
}
