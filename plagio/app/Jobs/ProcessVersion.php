<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
 
use ZipArchive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Proyecto;
use App\ArchivosProyecto;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Jobs\SendEmailJob;
class ProcessVersion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $datosProyecto_id;
    private $datosNotas;
    private $user_id;
    private $extension;
    private $ext;
    private $vers;
    protected $contenido;
    public function __construct($request,$id,$extension,$ext,$vers)
    {
        //dd($request);
        $this->datosProyecto_id = $request['proyecto_id'];
        $this->datosNotas = $request['notas'];
        $this->user_id = $id;
        $this->extension = $extension;
        $this->ext = $ext;
        $this->vers = $vers;
        //dd($this->datosRequest['archivo']);
    }

    public function handle()
    {
        $zip = new ZipArchive();
        $file =public_path().'/'.$this->extension; 
        $res = $zip->open($file);
        if ($res === TRUE && $this->ext == 'zip') {
            $proyecto = Proyecto::find($this->datosProyecto_id);
            $version = DB::table('archivosproyecto')->where('archivo',$proyecto->nombre)->count();
            //for($i = 0; $i < $zip->numFiles; $i++) {
                //$filename = $zip->getNameIndex($i);
                //$fileinfo = pathinfo($filename);
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.($version+1);
                //dd($route);
                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                //$zip->extractTo($route, array($zip->getNameIndex($i)));
                $zip->extractTo($route);
                //copy("zip://".$file."#".$filename, $route.'/'.$fileinfo['basename']);

                $dato = new ArchivosProyecto();
                //$dato->archivo = $fileinfo['basename'];
                $dato->archivo = $proyecto->nombre;
                $dato->proyecto_id = $this->datosProyecto_id;
                $dato->user_id = $this->user_id;
                $dato->version = $version+1;
                $dato->notas = $this->datosNotas;

                if($version >= 1)
                {
                    $dir1 = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.($version);
                    $dir2 = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.($version+1);

                    $dir1 = str_replace("\\","/",$dir1,$i);
                    $dir2 = str_replace("\\","/",$dir2,$i);

                    //dd($dir1);
                    $this->process_compare($dir2 , $dir1, 1);
                    //dd($this->contenido);
                }else{
                    //$resultadocomprar = null;
                    $dato->resultadocomprar = null;
                }
                $dato->resultadocomprar = serialize($this->contenido);
                $dato->save();

            $zip->close();
            unlink(public_path().'/archivo.zip');
            $user = User::find($this->user_id);
            
            $datos = ['email'=>$user->email,'mensaje'=>'El registro de la nueva version finalizo <br> para ver los resultados click en el siguiente boton','url'=>\URL::to('/').'/gestionversiones','user'=>$user];
            SendEmailJob::dispatch($datos);

        }else{
            //dd($this->ext);
            $zipFileName = 'Archivo.zip';
            $zip = new ZipArchive;
            $zip->open(public_path() . '/' . $zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $zip->addFile(public_path() . '/'.$this->extension,$this->extension);
            $zip->close();

            $zip2 = new ZipArchive;
            $zip2->open(public_path() . '/' . $zipFileName);
            $proyecto = Proyecto::find($this->datosProyecto_id);
            $version = DB::table('archivosproyecto')->where('archivo',$proyecto->nombre)->count();
            $route = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.($this->vers);

                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                //$zip->extractTo($route, array($zip->getNameIndex($i)));
                $zip2->extractTo($route);
                //copy("zip://".$file."#".$filename, $route.'/'.$fileinfo['basename']);
                $check = DB::table('archivosproyecto')->where('version',$this->vers)->exists();
                if(!$check)
                {                
                    $dato = new ArchivosProyecto();
                    //$dato->archivo = $fileinfo['basename'];
                    $dato->archivo = $proyecto->nombre;
                    $dato->proyecto_id = $this->datosProyecto_id;
                    $dato->user_id = $this->user_id;
                    $dato->version = $this->vers;
                    $dato->notas = $this->datosNotas;
                }else{
                    $a = DB::table('archivosproyecto')->where('version',$this->vers)->get();
                    $dato = ArchivosProyecto::find($a[0]->id);
                    //dd($dato);
                }

                if($version >= 1)
                {
                    $dir1 = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.((int)$this->vers - 1);
                    $dir2 = public_path() .'/proyectos2/'.$proyecto->nombre.'/version'.($this->vers);

                    $dir1 = str_replace("\\","/",$dir1,$i);
                    $dir2 = str_replace("\\","/",$dir2,$i);

                    //dd($dir1);
                    $this->process_compare($dir2 , $dir1, 1);
                    //dd($this->contenido);
                }else{
                    //$resultadocomprar = null;
                    $dato->resultadocomprar = null;
                }
                $dato->resultadocomprar = serialize($this->contenido);
                $dato->save();

            $zip2->close();
            unlink(public_path().'/'.$zipFileName);
            unlink(public_path().'/'.$this->extension);
            $user = User::find($this->user_id);
            
            $datos = ['email'=>$user->email,'mensaje'=>'El registro de la nueva version finalizo <br> para ver los resultados click en el siguiente boton','url'=>\URL::to('/').'/gestionversiones','user'=>$user];
            SendEmailJob::dispatch($datos);
            

        }
    }

    
    public function process_compare($dir1, $dir2, $only_check_has){
        $this->compare_file_folder($dir1,  $dir1, $dir2, $only_check_has);
    }
 
    public function compare_file_folder($dir1,  $base_dir1, $base_dir2, $only_check_has=0){
        
        if (is_dir($dir1)) {
            $handle = dir($dir1);
            if ($dh = opendir($dir1)) {
                while ($entry = $handle->read()) {
                    if (($entry != ".") && ($entry != "..")  && ($entry != ".svn")){
                        $new = $dir1."/".$entry;
                        
                        $other = preg_replace('#^'. $base_dir1 .'#' ,  $base_dir2, $new);
                        
                        if(is_dir($new)) {
                                                    // Comparar
                            if (!is_dir($other)) {
                                
                                $del = public_path() .'/proyectos2/';
                                $del = str_replace("\\","/",$del,$i);

                                $this->contenido [] = ['tipo' => 'directorio', 
                                                'mensaje' => 'No se encuentra el directorio',
                                                'comparado' => str_replace($del,'',$other)];
                                
                            }
                            $this->compare_file_folder($new, $base_dir1,$base_dir2,  $only_check_has) ;
                            
                                            } else {
                            if (!is_file($other)) {
                                
                                $del = public_path() .'/proyectos2/';
                                $del = str_replace("\\","/",$del,$i);
                                $this->contenido [] = ['tipo' => 'archivo', 
                                                'mensaje' => 'No se encuentra el archivo',
                                                'comparado' => str_replace($del,'',$other)];
                            }elseif ($only_check_has ==0 && ( md5_file($other) != md5_file($new) )  ){

                            }
                        }
                    }
                }
                closedir($dh);
                //closedir($dir1);
            }

        }
        //return serialize($contenido);
    }
}
