<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use ZipArchive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Proyecto;
use App\ArchivosProyecto;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Jobs\SendEmailJob;
class ProcessArchivo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $datosProyecto_id;
    private $datosNotas;
    private $user_id;
    private $extension;
    private $dirHere;
    private $version; 
    protected $contenido;

    public function __construct($request,$id,$extension)
    {
        //dd($request);
        $this->datosProyecto_id = $request['proyecto_id'];
        $this->datosNotas = '';
        $this->user_id = $id;
        $this->extension = $extension;
        $this->dirHere = $request['dirHere'];
        $this->version = $request['version'];

    }

    public function handle()
    {        
        $zip = new ZipArchive();
        $file = public_path().'/'.$this->extension; 
        $res = $zip->open($file);

        if ($res === TRUE) {
            $proyecto = Proyecto::find($this->datosProyecto_id);
            $version = DB::table('archivosproyecto')->where('archivo',$proyecto->nombre)->exists();
            //for($i = 0; $i < $zip->numFiles; $i++) {
                //$filename = $zip->getNameIndex($i);
                //$fileinfo = pathinfo($filename);
                $route = public_path() .'/proyectos2/'.$proyecto->nombre.'/'.$this->version;
                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                //$zip->extractTo($route, array($zip->getNameIndex($i)));
                $zip->extractTo($route);
                //copy("zip://".$file."#".$filename, $route.'/'.$fileinfo['basename']);
                if(!$version)
                {                
                    $dato = new ArchivosProyecto();
                    //$dato->archivo = $fileinfo['basename'];
                    $dato->archivo = $proyecto->nombre;
                    $dato->proyecto_id = $this->datosProyecto_id;
                    $dato->user_id = $this->user_id;
                    $dato->version = 1;
                    $dato->save();
                }

            $zip->close();
            unlink(public_path().'/'.$this->extension);
            
            $user = User::find($this->user_id);            
            $datos = ['email'=>$user->email,'mensaje'=>'El registro de los archivos finalizo <br> para ver los resultados click en el siguiente boton','url'=>\URL::to('/').'/archivoproyecto','user'=>$user];
            SendEmailJob::dispatch($datos);
        }else{
            // if($file != "undefined")
            // {
            //     $file = public_path().'/'.$this->extension;
            //     $nameFile = $file;
                
            //     $proyecto = Proyecto::find($this->datosProyecto_id);
            //     //dd($proyecto);
            //     //$version = DB::table('archivosproyecto')->where('archivo',$proyecto->nombre)->count();
            //     if($this->dirHere != ''){
            //         $directorio = str_replace("-","/",$this->dirHere,$i);
            //         $cadVec = explode("/", $directorio);
            //         //dd($cadVec);
            //         $contadores = array_count_values($cadVec);
            //         //dd($contadores,array_key_exists('..',$contadores)?$contadores['..']:0);
            //         $cantidad = array_key_exists('..',$contadores)?$contadores['..']:0;
            //         if($cantidad != 0)
            //         {                    
            //             while ($cantidad+1 != 0) {
            //                 $le = sizeof($cadVec);
            //                 unset($cadVec[$le-1]);
            //                 $cantidad--;
            //             }
            //         }
            //         $ruta = implode("/", $cadVec);
            //         //dd($ruta);

            //         //unset($listado[array_search('..', $listado, true)]);
            //         $path = public_path() .'/proyectos2/'. $ruta.'/'.$this->extension;
            //         //dd($path);
            //     }
            //     else
            //         $path = public_path() .'/proyectos2/'.$proyecto->nombre.'/'.$this->version.'/'.$this->extension;

            //     // if (!file_exists($path)) {
            //     //     mkdir($path, 0777, true);
            //     // }                
            //     $path = str_replace('\\','/', $path); 
            //     $nameFile = str_replace('\\','/', $nameFile);
            //     File::copy($nameFile,$path);
            //     dd($nameFile,$path);

            //     // if (!copy($,$)) {
            //     //     dd("Error al copiar");
            //     // }      

            //     // //$datos->imagen = $nameFile;

            //     // $dato = new ArchivosProyecto();
            //     // //$dato->archivo = $fileinfo['basename'];
            //     // $dato->archivo = $proyecto->nombre;
            //     // $dato->proyecto_id = $this->datosProyecto_id;
            //     // $dato->user_id = $this->user_id;
            //     // $dato->version = $version+1;
            //     // $dato->save();
            //     $user = User::find($this->user_id); 
            //     $datos = ['email'=>$user->email,'mensaje'=>'El registro del archivo finalizo <br> para ver los resultados click en el siguiente boton','url'=>\URL::to('/').'/archivoproyecto','user'=>$user];
            //     SendEmailJob::dispatch($datos);              
            // }
        }
    }
}
