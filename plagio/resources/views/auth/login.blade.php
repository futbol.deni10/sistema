<!DOCTYPE html>
<html lang="en">

<head>
    <title>LOGIN</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
        content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->

    <link rel="icon" href="{{asset('assets\files\assets\images\favicon.ico')}}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\bower_components\bootstrap\css\bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\icon\icofont\css\icofont.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\css\style.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\css\style.css')}}">
    
</head>

<body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    
                    <!-- Authentication card start -->
                    <form method="POST" action="{{ route('login') }}" class="md-float-material form-material" m>
                      @csrf
                        <div class="text-center">
                            <!-- <img src="..\files\assets\images\logo.png" alt="logo.png"> -->
                        </div>
                        <div class="auth-box card">

                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">SISTEMA PLAGIO</h3>
                                        @if(session('nopersona'))
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{session('nopersona')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="row m-b-20">
                                    <div class="col-md-6">
                                        <button class="btn btn-facebook m-b-20 btn-block"><i
                                                class="icofont icofont-social-facebook"></i>facebook</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-twitter m-b-20 btn-block"><i
                                                class="icofont icofont-social-twitter"></i>twitter</button>
                                    </div>
                                </div> -->
                                <!-- <p class="text-muted text-center p-b-5">Sign in with your regular account</p> -->
                                <div class="form-group form-primary">
                                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="login" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="usuario/email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <span class="form-bar"></span>
                                </div>
                                <div class="row m-t-25 text-left">
                                    <!-- <div class="col-12">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label>
                                                <input type="checkbox" value="">
                                                <span class="cr"><i
                                                        class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span class="text-inverse">Remember me</span>
                                            </label>
                                        </div>
                                        <div class="forgot-phone text-right f-right">
                                            <a href="auth-reset-password.htm" class="text-right f-w-600"> Forgot
                                                Password?</a>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                    
                                    <div class="col-md-12"> 
                                        <a class="btn btn-lg btn-google btn-block text-uppercase btn-outline" href="{{ url('auth/google') }}" >
                                            <img src="{{asset('image/google-logo.svg')}}" style="width: 15px"> <strong>Ingresar Con Google</strong></a> 
                                    </div>
                                    <div class="col-md-12 or-container">
                                        <div class="line-separator"></div>
                                        <div class="or-label">or</div>
                                        <div class="line-separator"></div>
                                    </div>
                                    <div class="col-md-12"> 
                                        <a class="btn btn-lg btn-google btn-block text-uppercase btn-outline" href="{{ url('auth/facebook') }}" >
                                            <img id="facebook1" src="https://image.flaticon.com/icons/svg/174/174848.svg"> <strong>Ingresar Con Facebook</strong></a> 
                                    </div>
                                            <div>
        <textarea rows="30" cols="50" id="editor" style="background-color:rgb(34, 29, 29);color:white;padding:10px" placeholder="Type Your Text..."></textarea>
        </div> 
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- Authentication card end -->
            </div>
            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->

        <!-- end of container-fluid -->
    </section>

    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="{{asset('assets\files\bower_components\jquery\js\jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets\files\bower_components\jquery-ui\js\jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets\files\bower_components\popper.js\js\popper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets\files\bower_components\bootstrap\js\bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets\files\assets\js\common-pages.js')}}"></script>
    <script src="{{asset('plugins/socket.io-client/dist/socket.io.js')}}"></script>
    <script type="text/javascript">
        
    
    var socket = io('http://localhost/veredaccion/1:3000');
    const log = console.log;

    const getEl = id => document.getElementById(id);

    const editor = getEl("editor");

    editor.addEventListener("keyup", evt => {
        console.log(editor.value);
        const text = editor.value;
        socket.send(text);
    });

    socket.on('message', data => {
        editor.value = data;
    });
    </script>
    <style type="text/css" media="screen">
        #infoMessage p {
            color: red !important
        }
        .btn-google {
            color: #545454;
            background-color: #ffffff;
            box-shadow: 0 2px 2px 3px #ddd;
        }
        .or-container {
            align-items: center;
            color: #ccc;
            display: flex;
            margin: 25px 0
        }

        .line-separator {
            background-color: #ccc;
            flex-grow: 5;
            height: 1px
        }
        .or-label {
            flex-grow: 1;
            margin: 0 15px;
            text-align: center
        }
        #facebook1
        {
            width: 15px;
        }

    </style>
</body>

</html>

