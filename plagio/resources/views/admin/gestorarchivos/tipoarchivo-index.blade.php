@extends('template.app')

@section('title','Tipo de Archivos Registrados')
@section('main-content')
<div class="row">
	<div class="col-md-6">
		<!-- if(Auth::user()->role->permission->add == 1) -->
        @if(Auth::user()->hasPermissionTo('GestorArchivo-create'))
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info" onclick="limpiarModal()" data-toggle="modal" data-target="#modal-Rtparchivo">
		        Nuevo Registro Tipo Archivo
		    </button>
		</div>
        @endif
		<!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-tparchivo" class="table table-bordered table-striped" style="width:100%">
		        <thead>
		            <tr>
		                <th>#</th>
		                <th>NOMBRE</th>
		                <th>CREDOR POR</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">

		        </tbody>
		    </table>
		</div>
	</div>
	<div class="col-md-6">
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info" onclick="limpiarModal()" data-toggle="modal" data-target="#modal-RExtension">
		        Nuevo Registro Extension
		    </button>
		</div>
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-Extension" class="table table-bordered table-striped" style="width:100%">
		        <thead> 
		            <tr>
		                <th>#</th>
		                <th>TIPO ARCHIVO</th>
		                <th>EXTENSION</th>
		                <th>ESTADO</th>
		                <th>CREDOR POR</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">

		        </tbody>
		    </table>
		</div>
	</div>
</div>

@include('admin.gestorarchivos.modal-crear-tparchivo')
@include('admin.gestorarchivos.modal-crear-extension')

@endsection
@section('js')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		activar_tabla_tparchivo();
		activar_tabla_extension();
		cambio();
	});

	function activar_tabla_tparchivo() {
    	var t = $('#Tabla-tparchivo').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_tparchivos') !!}',
        columns: [
            {data: 'id' },
            {data: 'nombre' },
            {data: 'nombreusuario' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';

                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Rtparchivo" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';              
                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-tparchivo').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 

	function activar_tabla_extension() {
		$('#Tabla-ArchivosP').DataTable().destroy();
    	var t = $('#Tabla-Extension').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_extension') !!}',
        columns: [
            {data: 'id' },
            {data: 'tipoarchivo' },
            {data: 'extension' },
            {data: 'estado',
            	render : function(data, type, row, meta){
            		if(row.estado == 'activo')
            			var html = '<span class="badge badge-success" style="width:80px;"> Activo </span>';
            		else
            			var html = '<span class="badge badge-danger" style="width:80px;"> Inactivo </span>';
            		return html;
            	}
        	},
        	{data: 'nombreusuario' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';
                var editar = "{{Auth::user()->hasPermissionTo('GestorArchivo-edit') }}";
                var eliminar = "{{Auth::user()->hasPermissionTo('GestorArchivo-delete') }}";
                if(eliminar != '')
                {

                	if(row.estado == 'activo')
                	{
                		html += '<a style="width:100px;" class="btn btn-danger btn-xs text-white" onclick="bajaExtension('+row.id+')" ><i class="fas fa-user-slash"></i> Dar De Baja</a> <br> ';
                	}else{
                		html += '<a style="width:100px;" class="btn btn-success btn-xs text-white" onclick="activarExtension('+row.id+')"><i class="fas fa-check"></i></span> Activar </a> <br>';
                	}
                }
                if(editar != '')
                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-RExtension" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEditExtension('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Extension').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 



	$("#btnRtparchivo").click(function(e)
	{
		$(".tipoarchivo").val($(".tipoarchivo").val().toUpperCase());
		var nombre = $(".tipoarchivo").val();
		var route = "{{route('archivo.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({
        	url: route,
	        headers: {'X-CSRF-TOKEN': token},
	        type: 'POST',
	        dataType: 'json',
	        data:{nombre},
	        success: function(data){
	            $("#modal-Rtparchivo").modal('toggle');
	            toastr.success("Registro Realizado");  
	            $('#Tabla-tparchivo').DataTable().ajax.reload();
	        },
	        error: function(data)
	        {
	        	toastr.error('ERROR');
	        }
        });
	})

	$("#btnEtparchivo").click(function(e){
		$(".tipoarchivo").val($(".tipoarchivo").val().toUpperCase());
		var nombre = $(".tipoarchivo").val();
		var criterio = $(".idtparchivo").val();
		var route = "{{url('archivo')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({
        	url: route,
	        headers: {'X-CSRF-TOKEN': token},
	        type: 'PUT',
	        dataType: 'json',
	        data:{nombre},
	        success: function(data){
	            $("#modal-Rtparchivo").modal('toggle');
	            toastr.success("Registro Realizado");  
	            $('#Tabla-tparchivo').DataTable().ajax.reload();
	        },
	        error: function(data)
	        {
	        	toastr.error('ERROR');
	        }
        });
	});


	$("#btnRExtension").click(function(e)
	{
		$(".extension").val($(".extension").val().toUpperCase());
		var extension = $(".extension").val();
		var tiposarchivo_id = $("#select_Rtparchivo").val();
		var route = "{{route('extension.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({
        	url: route,
	        headers: {'X-CSRF-TOKEN': token},
	        type: 'POST',
	        dataType: 'json',
	        data:{extension,tiposarchivo_id},
	        success: function(data){
	            $("#modal-RExtension").modal('toggle');
	            toastr.success("Registro Realizado");  
	            $('#Tabla-Extension').DataTable().ajax.reload();
	        },
	        error: function(data)
	        {
	        	toastr.error('ERROR');
	        }
        });
	});

	$("#btnEExtension").click(function(e)
	{
		$(".extension").val($(".extension").val().toUpperCase());
		var extension = $(".extension").val();
		var tiposarchivo_id = $("#select_Rtparchivo").val();
		var criterio = $(".idextension").val();
		var route = "{{url('updateExtension')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({
        	url: route,
	        headers: {'X-CSRF-TOKEN': token},
	        type: 'PUT',
	        dataType: 'json',
	        data:{extension,tiposarchivo_id},
	        success: function(data){
	            $("#modal-RExtension").modal('toggle');
	            toastr.success("Registro Realizado");  
	            $('#Tabla-Extension').DataTable().ajax.reload();
	        },
	        error: function(data)
	        {
	        	toastr.error('ERROR');
	        }
        });
	});

	function bajaExtension(criterio){
		swal({
            title: "CONFIRMAR",
            text: "Esta seguro dar de baja el registro seleccionado?",
            buttons: ["Cancelar", "Confirmar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = "{{url('bajaExtension')}}";
            $.ajax({
                    url: route,
                    method: 'get',
                    data:{id:criterio},
                    success: function(data) {
                        toastr.success("La Extension: "+data.extension+" Fue Dado de Baja");  
                        $('#Tabla-Extension').DataTable().ajax.reload();
                       
                    }
            });
          }  
        });
	}

	function activarExtension(criterio){
		swal({
            title: "CONFIRMAR",
            text: "Esta seguro de Activar el registro seleccionado?",
            buttons: ["Cancelar", "Confirmar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = "{{url('activarExtension')}}";
            $.ajax({
                    url: route,
                    method: 'get',
                    data:{id:criterio},
                    success: function(data) {
                    	console.log(data);
                        toastr.success("La Extension: "+data.extension+" Fue Activada");  
                        $('#Tabla-Extension').DataTable().ajax.reload();                       
                    }
            });
          }  
        });
	}

	function llenarDatosEdit(criterio)
	{
		var route = "{{url('archivo')}}/"+criterio+"/edit";
		$("#btnEtparchivo").show();
		$("#btnRtparchivo").hide();
		$.get(route , function(data){
			//console.log(data);
			$(".tipoarchivo").val(data.nombre);
			$(".idtparchivo").val(data.id);
		});
	}

	function llenarDatosEditExtension(criterio)
	{
		var route = "{{url('editExtension')}}/"+criterio;
		$("#btnEExtension").show();
		$("#btnRExtension").hide();
		$.get(route , function(data){
			//console.log(data);
			$("#select_Rtparchivo").val(data.tiposarchivo_id);
			$(".idextension").val(data.id);
			$(".extension").val(data.extension);
		});
	}
	$(document).on('show.bs.modal', '#modal-RExtension', function () {
		var route = "{{url('getArchivo')}}";
		$.get(route, function(data){
			$("#select_Rtparchivo").empty();
            $("#select_Rtparchivo").append(`<option value=""> </option>`);
            data.forEach(element => {
                $("#select_Rtparchivo").append(`<option value=${element.id}> ${element.nombre} </option>`);
            });
            $("#select_Rtparchivo").trigger("chosen:updated");
		});

	});

	function limpiarModal()
	{
		$("#btnEtparchivo").hide();
		$("#btnRtparchivo").show();

		$("#FRNtparchivo")[0].reset();
		$(".idtparchivo").val('');

		$("#btnEExtension").hide();
		$("#btnRExtension").show();

		$("#FRNextension")[0].reset();
		$(".idextension").val('');
	}

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
</script>
@endsection