@extends('template.app')

@section('title','Resultados de Plagio')
@section('main-content')

<div class="row">
	<div class="col-md-12">
		
		<div class="table-responsive">
		    <table id="Tabla-Resultado" class="table table-bordered table-striped" width="100%">
		        <thead> 
		            <tr>
		                <th>#</th>
		                <th>NOMBRE ARCHIVO A COMPARAR</th>
		                <th>FECHA</th>
<!-- 		                <th>EXTENSION</th>
		                <th>CREADO POR</th> -->
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">

		        </tbody>
		    </table>
		</div>
	</div>
</div>

@include('admin.comparararchivo.modal-chart')

@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script type="text/javascript" src="{{asset('plugins/docx/jszip/dist/jszip.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/docx/DOCX/docx.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('plugins/docx/main.js')}}"></script> -->
<script type="text/javascript">

	var textoExtraido;
	var verificar = false;

	jQuery(document).ready(function($) {
		cambio();
	}); 

	$( document ).ajaxStart(function() {
        $('.theme-loader').css('opacity','none');
        $('.theme-loader').show();
    }); 
 
	$( document ).ajaxStop(function() {
        $('.theme-loader').hide();
    });

	function activar_tabla_repositorio() {
    	var t = $('#Tabla-Resultado').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_resultados') !!}',
        columns: [
            {data: 'id' },
            {data: 'nombrearchivo' },
            {data: 'created_at' },
            // {data: 'extension' },
            // {data: 'nombreusuario' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';

                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Vchart" class="btn btn-warning btn-xs text-white" onclick="filtroMostrar('+row.id+');"><i class="fas fa-wrench"></i> Ver Resultado </a> </br>';

                    // html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="descargar('+row.id+');"><i class="fas fa-wrench"></i> Descargar </a> </br>'; 

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Resultado').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    activar_tabla_repositorio();
   
    var porcentaje;
    var cantidad;
    var criterio;
    function filtroMostrar(cri){
        criterio = cri;
    }
    
    $("#Select_Porcentaje").change(function(event) {
        $("#Select_Cantidad").prop({
            disabled: false
        })

        cantidad = $("#Select_Cantidad").val();
        porcentaje = event.target.value;
        if(cantidad != '' && porcentaje != '')
            VerResultado(criterio,cantidad,porcentaje);
        // else
        //     console.log('no');
    });

    $("#Select_Cantidad").change(function(event) {
        // $("#Select_Cantidad").prop({
        //     disabled: false
        // })
        console.log('cam');
        porcentaje = $("#Select_Porcentaje").val();
        cantidad= event.target.value;
        if(porcentaje != '' && cantidad != '')
            VerResultado(criterio,cantidad,porcentaje);
    });

	var chart;
	function VerResultado(id,cantidad,porcentaje)
	{ 
		var route = "{{url('getResultados')}}"; 
		var token = $('input[name="_token"]').val();
		$.ajax({
	        url: route,
	        headers: {'X-CSRF-TOKEN': token},
	        type: 'get',
	        dataType: 'json',
	        data:{id,cantidad,porcentaje},
            success : function(data)
            {	console.log(data);
            	if(data.tipo.nombre == "IMAGENES"){
                    if($("#ChartResultado").length > 0)
                    {
                        $(".doc canvas").remove();
                        $(".doc div").remove();
                    }
                    if($(".imag img").length > 0)
                    {                        
                        $(".imag div").remove();
                        $(".imag img").remove();
                    }
            	    var html = '';
            		//$("#modal-Vchart").modal('toggle');
                    for(var i=0; i<data.unoPor.length; i++)
                    {                 
                        if(data.unoPor[i].porcentaje >= data.porcentaje)
                        {                        
                    		var url = "{{URL::asset('archivos/IMAGENES')}}/"+ data.unoPor[i].extension+"/"+data.unoPor[i].urlRepositorio;
                            var url2 = "{{URL::asset('/')}}"+data.unoPor[i].urlComparar;
                            var url3 = "{{URL::asset('imagenes/comparadas')}}/"+data.unoPor[i].urlResultado;
                            html += '<div class="box-resultado">' 
                                html += '<div class=col-md-4>'              
                                html += '{!! Form::label("Nom", "Imagen Repositorio", []) !!}';
                        		html += '<img src="'+url+'" alt="" class="img-fluid">';
                                html += '</div>';
                                html += '<div class=col-md-4>'              
                                html += '{!! Form::label("Nom", "Imagen Comparar", []) !!}';
                                html += '<img src="'+url2+'" alt="" class="img-fluid">';
                                html += '</div>';
                                html += '<div class=col-md-4>' 
                                html += '{!! Form::label("Nom", "Imagen Resultado", []) !!} <br>';
                                html +=  data.unoPor[i].porcentaje +"% de similitud" ;
                                html += '<img src="'+url3+'" alt="" class="img-fluid">';
                                html += '</div>';
                            html += '</div>';

                            data.cantidad--;
                        }
                        if(data.cantidad == 0)
                            break;
                    }
            		$(".imag").append(html);
                    $(".titleResultado").html("Parte Roja: DISTINTO <BR> Parte Blanca: IGUAL");
                    $(".titleBuscar").html("Buscar Menor o Igual");
                    //alert('Parte roja = Diferente <br> , Parte blanca = Iguales')
            	}else
            	{
                    if($(".imag img").length>0)
                    {                        
                        $(".imag div").remove();
                        $(".imag img").remove();
                        //$(".imag label").remove();
                    }
                    if($("#ChartResultado").length>0)
                    {
                        $(".doc canvas").remove();
                        $(".doc div").remove();
                        var html =' <canvas id="ChartResultado"></canvas>';
                        $(".doc").append(html);
                    }else{                        
                        var html =' <canvas id="ChartResultado"></canvas>';
                        $(".doc").append(html);
                    }

            		$("#ChartResultado").load(location.href + " #ChartResultado");
            		var labels = [];
	            	var porcentaje = [];
	            	for(var i=0; i<data.unoPor.length; i++)
	            	{
                        if(data.unoPor[i].porcentaje >= data.porcentaje)
                        {
                            labels.push(data.unoPor[i].url);
                            porcentaje.push(Math.round(data.unoPor[i].porcentaje,2));
                            data.cantidad--;
                        }
                        if(data.cantidad == 0)
                            break;

	            	}
	            	//$("#modal-Vchart").modal('toggle');
	            	var ctx = document.getElementById('ChartResultado').getContext('2d');
					chart = new Chart(ctx, {
					    // The type of chart we want to create
					    type: 'bar',

					    // The data for our dataset
					    data: {
					        labels: labels,
					        datasets: [{
					            label: 'Resultado de Archivos Comparados',
					            backgroundColor: 'rgb(255, 99, 132,0.8)',
					            borderColor: 'rgb(255, 99, 132)',
					            data: porcentaje,
					            borderWidth: 1
					        }]
					    },

					    // Configuration options go here
					    options: {
					        scales: {
					            yAxes: [{
					                ticks: {
					                    beginAtZero: true
					                }
					            }]
					        },
					        animation: {
							  onComplete: function () {
							    var chartInstance = this.chart;
							    var ctx = chartInstance.ctx;
							    //console.log(chartInstance);
							    var height = chartInstance.controller.boxes[0].bottom;
							    ctx.textAlign = "center";
							    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
							      var meta = chartInstance.controller.getDatasetMeta(i);
							      Chart.helpers.each(meta.data.forEach(function (bar, index) {
							        //ctx.fillText(dataset.data[index]+"%", bar._model.x, height - ((height - bar._model.y) / 2));
							        ctx.fillText(dataset.data[index]+"%", bar._model.x, 100);
							      }),this)
							    }),this);
							  }
							}
						},

					});
                    $(".titleResultado").html("Resultado de documento Comparados");
            	}
            },
            error : function(data)
            {
            	toastr.error("ERROR");
            	console.log(data);
            }

		});
	}

	$('#modal-Vchart').on('hidden.bs.modal', function () {
        $("#FRNchart").trigger('reset');
        // $("#Select_Porcentaje").val('');
        // $("#Select_Cantidad").val('');

        $("#Select_Cantidad").prop({
                disabled: true
            });
		if(chart)
		{
            chart.destroy();
            $(".doc canvas").remove();
            $(".doc div").remove();
        }
        if($(".imag img").length>0)
        {                        
            $(".imag div").remove();
            $(".imag img").remove();
            //$(".imag label").remove();
        }
	})

		function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };	 
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
    .box-resultado{
        padding: 5px;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        border-bottom: 1px solid #5f5f5f38;
    }
</style>
@endsection