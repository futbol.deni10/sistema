<div class="modal fade" id="modal-Vchart" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable">
		{!! Form::open(['id'=> 'FRNchart','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title titleResultado" >RESULTADOS</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{!! Form::label('type','Mostrar Mayor o Igual a:',['id'=>'titleBuscar']) !!}
				            {!! Form::select(
				                'porcentaje',$porcentaje,
				                null,
				                ['class'=>'form-control',
				                'placeholder'=>'Seleccione una Opcion...',
				                'id'=>'Select_Porcentaje']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('type','Mostrar la cantidad De:') !!}
				            {!! Form::select(
				                'cantidad',$cantidad,
				                null,
				                ['class'=>'form-control',
				                'placeholder'=>'Seleccione una Opcion...',
				                'id'=>'Select_Cantidad' ,'disabled']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12 cuerpo">					
							<div class="imag row">
								
							</div>
							<div class="doc">
								
							</div>
						</div>
					</div>
				</div>		
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>