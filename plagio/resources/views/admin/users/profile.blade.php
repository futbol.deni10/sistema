@extends('template.app')

@section('title','PERFIL')
@section('main-content')
<div id="boxDatos">
<div class="col-lg-12">    
    <div class="cover-profile">
        <div class="profile-bg-img">
            <img class="profile-bg-img img-fluid" src="{{asset('image/bg-img1.jpg')}}" alt="bg-img">
            <div class="card-block user-info">
                <div class="col-md-12">
                    <div class="media-left">
                        <a href="#" class="profile-image">
                            <img class="user-img img-radius" src="{{asset('assets/files/assets/images/avatar-4.jpg')}}" alt="user-img">
                        </a>
                    </div>
                    <div class="media-body row">
                        <div class="col-lg-12">
                            <div class="user-title">
                                <h2>{{$user->nombre}} {{$user->apellido}}</h2>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="tab-header card">
            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Informacion Personal</a>
                    <div class="slide"></div>
                </li>                                            
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="personal" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Acerca de mi</h5>
                        
                    </div>
                    <div class="card-block">
                        <div class="view-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">Nombre Completo</th>
                                                                <td>{{$user->nombre}} {{$user->apellido}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">CI</th>
                                                                <td>{{$user->ci}} </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Direccion</th>
                                                                <td>{{$user->direccion}} </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- end of table col-lg-6 -->
                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">Email</th>
                                                                <td><a href="#!"><span class="__cf_email__" data-cfemail="4206272f2d02273a232f322e276c212d2f">{{$user->email}}</span></a></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Celular</th>
                                                                <td>{{$user->telefono}} </td>
                                                            </tr>
                                                          
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- end of table col-lg-6 -->
                                        </div>
                                        
                                        <!-- end of row -->
                                    </div>
                                    <div align="left" style="border:auto;">
                                        <button type="button" class="btn btn-info" onclick="llenarDatos('{{$user->id}}')" data-toggle="modal" data-target="#modal-RUsers">
                                            Actualizar Datos
                                        </button>
                                    </div>
                                    <!-- end of general info -->
                                </div>
                                <!-- end of col-lg-12 -->
                            </div>
                            <!-- end of row -->
                        </div>

                    </div>                        
                    </div>
                </div>
            </div>
        </div>
        <!-- tab content end -->
    </div>
</div>
    
</div>

<!-- Modal Usuario--> 
<div class="modal fade" id="modal-RUsers" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog ">
        {!! Form::open(['id'=> 'FRUsers','autocomplete' => 'off']) !!}
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="staticBackdropLabel" style="margin-left: 20%;">Actualizar Datos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display: none;">
                    <div class="row">
                        <div class="col-md-6">
                        {!! Form::label('username','Usuario') !!}
                        {!! Form::text( 
                            'username',null,
                            ['class'=>'form-control',
                            'placeholder' =>'Nombre del Usuario',
                            'id'=>'Rusername']) !!}
                        </div>
                        <div class="col-md-6" >
                        {!! Form::label('email','Correo Electronico') !!}
                        {!! Form::email(
                            'email',null,
                            ['class'=>'form-control',
                            'placeholder' =>'Example@gmail.com',
                            'id'=>'Remail']) !!}
                        </div>
                    </div> 
                </div>
                <div class="form-group"> 
                    <input type="hidden" class="idU" value="">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('nombre','Nombres') !!}
                            {!! Form::text('nombre',null, 
                                ['class'=>'form-control nombre',
                                'placeholder' =>'Nombre de la Persona',
                                'id'=>'RPnombre']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('apellido','Apellidos') !!}
                            {!! Form::text('apellido',null, 
                                ['class'=>'form-control apellido',
                                'placeholder' =>'Apellido de la Persona',
                                'id'=>'RPapellido']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('ci','Carnet de Identidad') !!}
                            {!! Form::text('ci',null, 
                                ['class'=>'form-control ci',
                                'placeholder' =>'Carnet de Identidad',
                                'id'=>'RPci','onkeypress'=>"return valida(event,true)"]) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('ciudad_id','Extension') !!}
                            <select class='form-control select_Pciudad' name="ciudad_id" id="Plist_ciudad">
                                <option value=""></option>
                            </select> 
                        </div>
                    </div>
                </div>              
                <div class="form-group">
                    <div class="row">       
                        <div class="col-md-6">
                        {!! Form::label('direccion','Direccion') !!}
                        {!! Form::textarea( 
                            'direccion',null,
                            ['class'=>'form-control direccion',
                            'placeholder' =>'direccion',
                            'id'=>'RPdireccion']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('celular','Nº Telefono') !!}
                            {!! Form::text('telefono',null, 
                                ['class'=>'form-control telefono',
                                'placeholder' =>'+59170045677',
                                'id'=>'RPtelefono','onkeypress'=>"return validaTelefono(event,true)"]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {!!link_to('#', $title='Actualizar Datos', $attributes = ['id'=>'btnRUser', 'class'=>'btn btn-primary'])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div> 

@endsection
@section('js') 
<script type="text/javascript">
    $( document ).ready(function() {
        llenarCiudades();
        setTimeout(function(){ 
            cambio();
        }, 200);

    });

    function llenarDatos(criterio){
        var route = "{{url('users')}}/"+criterio+"/edit";
        $.get(route, function(data){
            $("#Rusername").val(data.username);
            $("#Remail").val(data.email);
            $("#RPnombre").val(data.nombre)
            $("#RPapellido").val(data.apellido)
            $("#RPci").val(data.ci)
            $("#Plist_ciudad").val(data.ciudad_id)
            $("#RPdireccion").val(data.direccion)
            $("#RPtelefono").val(data.telefono)
            $(".idU").val(data.id)
        });
    }

    function llenarCiudades()
    {
        var route = "{{url('get_allciudades')}}";
        $.get(route,function(res, sta){
            $("#Plist_ciudad").empty();
            $("#Plist_ciudad").append(`<option value=""> </option>`);
            res.forEach(element => {
                $("#Plist_ciudad").append(`<option value=${element.id}> ${element.nombre} </option>`);
            });
            $("#Plist_ciudad").trigger("chosen:updated");
        });

        $("#Rusername").hide();
        $("#Remail").hide();
        $(".dause").hide();
    }

    $("#btnRUser").click(function(event) {
        var id = $(".idU").val();
        var registerForm = $("#FRUsers");
        var formData = registerForm.serialize();
        var route = "{{url('actualizardatos/')}}/"+id+"";
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
        type: 'put',
        dataType: 'json', 
        data:formData,
        success: function(data){
            $("#modal-RUsers").modal('toggle');
            toastr.success("Datos Actualizados");
            $( "#boxDatos" ).load(window.location.href + " #boxDatos" );
            //$("#DataTableUser").DataTable().ajax.reload();
        }, 
        error:function(data)
        {
            console.log(data);
        }  
        });
    });

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
    </script>
<style type="text/css">
    textarea{
        resize: none;
        height: 150px;
    }
</style>
@endsection
