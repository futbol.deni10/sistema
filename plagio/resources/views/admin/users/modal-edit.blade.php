<div class="modal fade" id="modal-EUsers" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    {!! Form::open(['id'=> 'FREUsers','autocomplete' => 'off']) !!}
    <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title" id="ETitle-Label" style="margin-left: 20%;">Formulario - Editando Usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" id="Eid">
          <div class="form-group">
            <div class="col-xs-6">
            {!! Form::label('username','Usuario') !!}
            {!! Form::text(
              'username',null,
              ['class'=>'form-control',
              'placeholder' =>'Nombre del Usuario',
              'required','id'=>'Eusername']) !!}
            </div>
            <div class="col-xs-6">
            {!! Form::label('email','Correo Electronico') !!}
            {!! Form::email(
              'email',null,
              ['class'=>'form-control',
              'placeholder' =>'Example@gmail.com',
              'required','id'=>'Eemail']) !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-6">
            {!! Form::label('role_id','Tipo') !!}
            {!! Form::select(
              'roles',$roles,
             null,
              ['class'=>'form-control',
              'placeholder'=>'Seleccione una Opcion...',
              'required','id'=>'Erole_id']) !!}
            </div>
          </div>
          <div class="form-group"> 
            <div class="row">
              <div class="col-md-6">
                  {!! Form::label('nombre','Nombres') !!}
                  {!! Form::text('nombre',null, 
                    ['class'=>'form-control nombre',
                  'placeholder' =>'Nombre de la Persona',
                  'id'=>'EPnombre']) !!}
              </div>
              <div class="col-md-6">
                  {!! Form::label('apellido','Apellidos') !!}
                  {!! Form::text('apellido',null, 
                    ['class'=>'form-control apellido',
                  'placeholder' =>'Apellido de la Persona',
                  'id'=>'EPapellido']) !!}
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                  {!! Form::label('ci','Carnet de Identidad') !!}
                  {!! Form::text('ci',null, 
                    ['class'=>'form-control ci',
                  'placeholder' =>'Carnet de Identidad',
                  'id'=>'EPci','onkeypress'=>"return valida(event,true)"]) !!}
              </div>
              <div class="col-md-6">
                  {!! Form::label('ciudad_id','Extension') !!}
                  {!! Form::select('ciudad_id', $ciudades, null, ['class'=>'form-control', 'id'=>'Elist_ciudad' ]) !!}
                  
              </div>
            </div>
          </div>        
          <div class="form-group">
            <div class="row">   
              <div class="col-md-6">
              {!! Form::label('direccion','Direccion') !!}
              {!! Form::textarea( 
                'direccion',null,
                ['class'=>'form-control direccion',
                'placeholder' =>'direccion',
                'id'=>'EPdireccion']) !!}
              </div>
              <div class="col-md-6">
                {!! Form::label('celular','Nº Telefono') !!}
                  {!! Form::text('telefono',null, 
                    ['class'=>'form-control telefono',
                  'placeholder' =>'+59170045677',
                  'id'=>'EPtelefono','onkeypress'=>"return validaTelefono(event,true)"]) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'btnEUsers', 'class'=>'btn btn-primary'])!!}
        </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>