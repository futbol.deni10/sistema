<div class="modal fade" id="modal-verVersion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		{!! Form::open(['id'=> 'FRNVversion','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="text-align: center;">Gestion de Versiones</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="dirHere" value="">
			    <input type="hidden" class="dirOriginal" value="">
			    <input type="hidden" class="criterio" value="">
			    <input type="hidden" class="ini" value="">
				<div class="row"> 
					<div class="col-md-12">
						<div align="left" style="border:auto;">
						    <button type="button" class="btn btn-warning"  onclick="BuscarComparar();">
						        COMPARAR ARCHIVOS
						    </button>
						</div>
						<hr>
						<div class="table-responsive">
						    <table id="Tabla-ArchivosP" class="table table-bordered table-striped" width="100%">
						        <thead> 
						            <tr>
						                <th>#</th>
						                <th>CHECK</th>
						                <th>ARCHIVOS</th>
				                        <th>TIPO ARCHIVO</th>
				                        <th>SUBIDO POR</th>
				                        <th>FECHA CREACION</th>
						            </tr>
						        </thead>
						        <tbody style="text-align: left;">
				 
						        </tbody>
						    </table>
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

