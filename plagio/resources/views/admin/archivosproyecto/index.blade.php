              @extends('template.app')

@section('title','Lista Archivos de Proyectos')
@section('main-content')
    <!-- icon -->
<!-- <link rel="stylesheet" href="{{asset('assets\files\assets\icon\icofont\css\icofont.css')}}"> -->
<!-- <link rel="stylesheet" type="text/css" href="{{asset('assets\files\bower_components\jstree\css\style.min.css')}}" -->
<!-- <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\pages\treeview\treeview.css')}}"> -->
<link href="{{ asset('plugins/font-awesome/css/all.css')}}" rel="stylesheet" />
@if(count($datos) > 0)
<div class="row"> 
    <div class="col-md-12">
        <!-- if(Auth::user()->role->permission->add == 1) -->
        <div align="left" class="btnCaja" style="border:auto;">
            <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rarchivo">
                REGISTRAR ARCHIVO
            </button>
                    
            <button type="button" class="btn btn-warning"  onclick="BuscarComparar();">
                COMPARAR ARCHIVOS
            </button>
 
        </div>
        <!-- endif -->
    </div> 
    <input type="hidden" class="dirHere" value="">
    <input type="hidden" class="dirOriginal" value="">
    <input type="hidden" class="criterio" value="">
    <input type="hidden" class="ini" value="">
    <div class="col-md-6">
        <div align="left" style="border:auto;">
            {!! Form::label('type','Proyecto Asignado') !!}
            {!! Form::select(
                'proyecto_id',$datos,
                null,
                ['class'=>'form-control',
                'placeholder'=>'Seleccione una Opcion...',
                'id'=>'Select_Pasignado']) !!}           
        </div>
    </div>
</div>
@else
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    No tiene ningun proyecto asignado
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<hr>
<div class="mensaje">
    
</div>
<!-- <div class="card-block tree-view">
    <div id="basicTree2" >

    </div>
</div> -->
<div class="row"> 
	<div class="col-md-12">
		<div class="table-responsive">
		    <table id="Tabla-ArchivosP" class="table table-bordered table-striped" width="100%">
		        <thead> 
		            <tr> 
		                <th>#</th>
                        <th>check</th>
		                <th>ARCHIVOS</th>
                        <th>TIPO ARCHIVO</th>
                        <th>FECHA CREACION</th>
                        <th>SUBIDO POR</th>
                        <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: left;">
 
		        </tbody>
		    </table>
		</div>
	</div>
</div>

@include('admin.archivosproyecto.modal-crear')
@endsection

@section('js')
<script type="text/javascript" src="{{asset('plugins/docx/jszip/dist/jszip.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/docx/DOCX/docx.js')}}"></script>
<script type="text/javascript">
    $( document ).ajaxStart(function() {
        $('.theme-loader').css('opacity','none');
        $('.theme-loader').show();
    }); 
 
    $( document ).ajaxStop(function() {
        $('.theme-loader').hide();
    });

    jQuery(document).ready(function($) {
        cambio();
    });

    function activar_tabla_directorio(directorio,criterio,ini,original)
    {
        //console.log(directorio,original,ini,criterio);
        $(".dirHere").val(directorio);
        $(".dirOriginal").val(original);
        $(".criterio").val(criterio);
        $(".ini").val(ini);
        $('#Tabla-ArchivosP').DataTable().destroy();
        var route = "{{url('listadoDirectorio')}}/"+directorio+"/"+criterio+"/"+ini+"/"+original;
        var t = $('#Tabla-ArchivosP').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":route,
            columns: [
                {data: null ,
                  render :  function(data, type, row, meta){
                    //console.log(data)
                    if(data.mensaje != '')
                    {
                      var htmlMensaje = '<div class="alert alert-danger alert-dismissible fade show" role="alert">'+row.mensaje+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                    '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                            '</div>';
                      $(".mensaje div").remove();
                      $(".mensaje").append(htmlMensaje);  
                    }else
                    {
                      $(".mensaje div").remove(); 
                    }
                    return null;
                  }
                },
                {data: null ,
                    "render" : function(data, type, row, meta) {
                        var html = '';
                        if(row.tipo == 'file' )
                        { 
                            if(row.dir.split('.').pop() == 'docx' || row.dir.split('.').pop() == 'pdf' || row.dir.split('.').pop() == 'xlsx' || row.dir.split('.').pop() == 'png' ||  row.dir.split('.').pop() == 'jpg' || row.dir.split('.').pop() == 'jpeg'){
                                var nombre =row.dir+'-'+row.directorio+'-version1-'+ $( "#Select_Pasignado option:selected" ).text();                          
                                var html='<div class="form-group" >';
                                    html+='<input type="checkbox" autocomplete="off" value="'+nombre+'">';
                                html +="</div>";
                            }
                        }
                    return html;
                    },
                },
                {data: 'dir' ,
                    render :  function(data, type, row, meta){
                        var html = '';
                        if(row.tipo == 'dir' )
                        {        
                            var direc =  row.directorio.replace(/[/]/g, '-')
                            html += '<i class="fas fa-folder-open"></i> '; 
                            html += '<a href="#" class="stretched-link" onclick="activar_tabla_directorio(`'+direc+'`,`'+row.criterio+'`,2,`version1`)">'+row.dir+'</a>'                            
                        }else{
                            if(row.tipo == 'file' )
                            {

                                var direc =  row.directorio.replace(/[/]/g, '-')
                                html += '<i class="far fa-file"></i> ';
                                html += '<a href="#" class="stretched-link" onclick="descargar(`'+direc+'`,`'+row.dir+'`)">'+row.dir+'</a>' 
                            }

                        }

                        return html;
                    }
                },       
                {data: 'tipoarchivo'},       
                {data: 'fecha'},       
                {data: 'usuario'},
                {
                "defaultContent" : " ",
                "searchable" : false,
                "orderable" : false,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                        var remove = "{{Auth::user()->hasPermissionTo('GestorArchivoProyecto-delete') }}";
                        if(remove !='')
                        {
                            if(row.directorio != ''){
                                
                            html+='<a class="btn btn-danger btn-xs text-white" onclick="eliminarArchivo(`'+row.dir+'`);"><i class="fas fa-wrench"></i> Eliminar </a> </br>'; 
                            }
                        }

                    html +="</div>";
                    return html;
                }
                },  
            ]
        });

        t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-ArchivosP').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }

    var datosComparar = [];
    function BuscarComparar()
    {
        $('#Tabla-ArchivosP tbody tr').each(function(index, value){
            if( $(this).find('input').prop('checked') ) {
                datosComparar.push($(this).find('input')[0].value);
                //datosComparar.push($(this).find('input')[0].value);
            }            
        });
        if(datosComparar.length > 0){
            var route = "{{url('leerDocumento2')}}"; 
            var token = $('input[name="_token"]').val();
            $.ajax({ 
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{datosComparar},
                success : function(data)
                {
                    datosComparar = [];
                    toastr.success('Estado en espera');
                },
                error : function(data)
                {
                    toastr.error("ERROR");
                    console.log(data);
                }

            });
        }else
            toastr.error('Debe seleccionar almenos un archivo para enviar a deteccion de PLAGIO');

    }


    $("#Select_Pasignado").change(function(e){
        var criterio = $("#Select_Pasignado").val();
        var id = $( "#Select_Pasignado option:selected" ).text()+'-version1';
        if(criterio != "")
          activar_tabla_directorio(id,criterio,1,id);
        else
          activar_tabla_directorio(id,0,1,id);
    })
        
    var textoExtraido;
    var verificar = false;
    var pt = 0;
    var ca = 0;
    function convert() {        
        var cant_archivos = $('#RFarchivo').fileinput('getFileStack').length;
        for(var i=0; i<cant_archivos; i++){
            //pt = i;
            ca = cant_archivos;
            var selected_file =  $('#RFarchivo').fileinput('getFileStack')[i];
            //var selected_file =  "{{asset('Archivo.docx')}}"
            var reader = new FileReader();
            reader.onload = function(aEvent) {
                textoExtraido = convertToPDF(btoa(aEvent.target.result));
               // console.log(textoExtraido);
                if(pt+1 == ca)
                    enviarDocumento(textoExtraido,pt,true);
                else
                    enviarDocumento(textoExtraido,pt,false);
                pt++;
            };             
            reader.readAsBinaryString(selected_file);
        }

    }
    
    function convertToPDF(aDocxContent) {
        try {
            var content = docx(aDocxContent);
            //$id('container').textContent = '';
            //console.log('content length: ' + content.DOM.length);
            var node ="";
            var con = [];
            for(var i=0; i<content.DOM.length; i++) {
                node = content.DOM;
            }
            for(var j=0; j<node.length; j++) {
                con [j]= node[j].outerHTML;
            }
            verificar = true;

          //$id('container').appendChild(node[0]);
            return con;
        }
        catch (e) {
           return null;
        }
        
    }

    window.addEventListener('load', function() {
        document.getElementById('btnRarchivo').onclick = convert;
        //cargarextension();
        //console.log('hola');
    });

    function enviarDocumento(criterio,pos,sem){
        //console.log(criterio,pos,sem);
        var archivo = $('#RFarchivo').fileinput('getFileStack')[pos];
        var proyecto_id = $("#Select_Pasignado").val();
        var dirHere = $(".dirHere").val();
        var dirOriginal =$(".dirOriginal").val();
        var version = 'version1';
        var formData = new FormData();
        formData.append('archivo',archivo);
        formData.append('proyecto_id',proyecto_id); 
        formData.append('dirHere',dirHere); 
        formData.append('dirOriginal',dirOriginal); 
        formData.append('version',version); 
        formData.append('verificar',verificar);
        formData.append('criterio',criterio);
        var route = "{{route('archivoproyecto.store')}}";
        var token = $('input[name="_token"]').val();
        if(proyecto_id != ''){
            $.ajax({
            url: route, 
            headers: {'X-CSRF-TOKEN': token},
            method: 'POST',
            data:formData,
            contentType: false,
            cache: false,
            processData : false,
            success : function(data){
                if(sem){
                    $("#modal-Rarchivo").modal('toggle');
                    toastr.success("Archivo Almacenado");
                    activar_tabla_directorio($(".dirHere").val(),$(".criterio").val(),$(".ini").val(),$(".dirOriginal").val());
                }
            },
            error : function(data){
                toastr.error("ERROR");

            }
            });
        }else {
           toastr.error("Debe seleccionar un proyecto"); 
        }
    };

    function descargar(criterio,name){
        var url = "{{url('getDocumento')}}/"+criterio+"/"+name;
        window.open(url, '_blank');
    }

    function eliminarArchivo(dir){
        var dirhere = $(".dirHere").val();
        var route = "{{url('eliminarArchivo')}}/"+dir+'/'+dirhere;
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url:route,
            method:'get',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            //data: {nombre},
            success : function(data){
                //$("#modal-Rproyecto").modal('toggle');
                toastr.success('Archivo Eliminado');
                $("#Tabla-ArchivosP").DataTable().ajax.reload();
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    }

    $('#RFarchivo').fileinput(
    {
        language: 'es',
        dropZoneEnabled: false,
        showUpload : false,
        //allowedFileExtensions: ['zip']
    });

    $("#btnRproyecto").click(function(){
        $("#Rnombre").val($("#Rnombre").val().toUpperCase());
        var nombre = $("#Rnombre").val();
        var route = "{{route('proyectos.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url:route,
            method:'POST',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            data: {nombre},
            success : function(data){
                $("#modal-Rproyecto").modal('toggle');
                toastr.success('Proyecto Registrado');
                $("#Tabla-ArchivosP").DataTable().ajax.reload();
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    });

    function estadoProyecto(id,criterio)
    {
        console.log(criterio);
        if(criterio == 1)
            var texto = "Esta seguro dar de baja el registro seleccionado?"
        else
            var texto = "Esta seguro de Activar el registro seleccionado?"
        swal({
            title: "CONFIRMAR",
            text: texto,
            buttons: ["Cancelar", "Confirmar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = "{{url('estadoProyecto')}}";
            $.ajax({
                url: route,
                method: 'get',
                data:{id,criterio},
                success: function(data) {
                    toastr.success(data);  
                    $('#Tabla-Proyecto').DataTable().ajax.reload();
                   
                }
            });
          }  
        });
    }


    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
</style>
@endsection