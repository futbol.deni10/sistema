<div class="modal fade" id="modal-Easignacion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNEasignacion','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" >Editar Asignacion</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" id="criterioAsignacion" value="">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Proyectos') !!}
							{!! Form::select(
								'proyecto_id',$proyectos,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Eproyecto']) !!} 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('fecha','Fecha de expiración') !!}
						  	{!! Form::text('tiempo',null, 
						  		['class'=>'form-control' ,'id'=>'Efecha']) !!}
							
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Usuarios') !!}
							{!! Form::select(
								'user_id',$users,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Euser']) !!} 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Roles') !!}
							{!! Form::select(
								'roleproyecto_id',$Roles,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Erole']) !!} 
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar Asignacion', 
				$attributes = ['id'=>'btnEasignacion', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>