<div class="modal fade" id="modal-Rasignacion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNasignacion','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" >Formulario Nueva Asignacion</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Proyectos') !!}
							{!! Form::select(
								'proyecto_id',$proyectos,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Rproyecto']) !!} 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('fecha','Fecha de expiración') !!}
						  	{!! Form::text('tiempo',null, 
						  		['class'=>'form-control' ,'id'=>'fecha']) !!}
							
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Usuarios') !!}
							{!! Form::select(
								'user_id',$users,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Ruser']) !!} 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Roles') !!}
							{!! Form::select(
								'roles',$Roles,
								null,
								['class'=>'form-control',
								'placeholder'=>'Seleccione una Opcion...',
								'id'=>'Rrole']) !!} 
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Asignacion', 
				$attributes = ['id'=>'btnRasignacion', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>