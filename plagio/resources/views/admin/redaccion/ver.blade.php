@extends('template.app')

@section('title','Redaccion TEXTO - '.$who)
@section('main-content')
<link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        @if(Auth::user()->hasPermissionTo('Redaccion-create'))
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info" onclick="estilo();" data-toggle="modal" data-target="#modal-Rredaccion">
                Asignar Usuario
            </button>
        </div>
        @endif
    </div>
    <div class="col-md-12" style="text-align: -webkit-center;">
        <div class="col-xl-3 col-md-6" style="text-align: center;">
            <div class="card bg-c-pink update-card">                
                <div class="card-footer">
                    <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>{{$who}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<input type="hidden" class="criterio" value="{{$criterio->id}}">
<div class="row">
	<div class="col-md-12">
        <textarea name="content" id="editor">
            {!! $criterio->contenido !!}
        </textarea>
        <div>
        <textarea rows="30" cols="50" id="editor" style="background-color:rgb(34, 29, 29);color:white;padding:10px" placeholder="Type Your Text..."></textarea>
        </div>
	</div>
</div>
@include('admin.redaccion.modal-crear')
@endsection

@section('js')
<script src="{{asset('plugins/ckeditor5/build/ckeditor.js')}}"></script> 
<!-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> -->
<script src="{{asset('plugins/select2/select2.min.js')}}"></script> 
<script src="{{asset('plugins/select2/maximize-select2-height.js')}}"></script> 
<script src="{{asset('plugins/socket.io-client/dist/socket.io.js')}}"></script>
<!-- <script src="./client.js"></script> -->
<script type="text/javascript">
    
    var socket = io('http://localhost:3000');
    const log = console.log;

    const getEl = id => document.getElementById(id);

    const editor = getEl("editor");

    editor.addEventListener("keyup", evt => {
        console.log(editor.value);
        const text = editor.value;
        socket.send(text);
    });

    socket.on('message', data => {
        editor.value = data;
    });
// const log = console.log;

// const getEl = id => document.getElementById(id);

// const editor = getEl("editor");

// editor.addEventListener("keyup", evt => {
//     const text = editor.value;
//     socket.send(text);
// });

// socket.on('message', data => {
//     editor.value = data;
// });

// io.on('disconnect', evt => {
//     log('some people left');
// });

    //const log = console.log;

    //var socket = io('http://localhost:8000');
    // const log = console.log;

    // const getEl = id => document.getElementById(id);

    // const editor = getEl("editor");

    // editor.addEventListener("keyup", evt => {
    //     const text = editor.value;
    //     socket.send(text);
    // });

    // socket.on('message', data => {
    //     editor.value = data;
    // });
    //Pusher.logToConsole = true;
    // var pusher = new Pusher('2112368c6a535325d888', {
    //     cluster: 'us2',
    //     encrypted: true
    // });

//    var channel = pusher.subscribe('nuevaorden');

    // channel.bind('App\\Events\\NuevaOrdenEvent', function(data) {
    //     //toastr.success(data.message);
    //     //$("#mensajeSuccess").html('')
    //     //console.log(data);
        
    //     window.editor.setData(data.message);
    //     window.editor.model.change( writer => {
    //         writer.setSelection( writer.createPositionAt( window.editor.model.document.getRoot(), 'end' ));
    //     });
        
    // });

    var contenido = '';
    var cont = 5;
    var ckedit;
    var aux = 0;
    // ClassicEditor
    // .create( document.querySelector( '.editor' ),{
    //     // autosave: {
    //     //     save( editor ) {
    //     //         //verCambio(editor);
    //     //         //len = contenido.length;
    //     //         //return contenido = editor.getData() ;
    //     //     }
    //     // },
    //     toolbar: {
    //         items: [
    //             'heading',
    //             '|',
    //             'bold',
    //             'italic',
    //             'link',
    //             'bulletedList',
    //             'numberedList',
    //             '|',
    //             'outdent',
    //             'indent',
    //             '|',
    //             'blockQuote',
    //             'undo',
    //             'redo'
    //         ]
    //     },
    //     language: 'es',
    //     licenseKey: '',        
    // })
    // .then( editor => {
    //     window.editor = editor;
    //     ckedit = editor;
    //     contenido = editor.getData();
    //     editor.model.document.on('change:data', (evt, data) => {
    //         if(aux == 10){
    //             aux = 0;
              
    //                 // $('.theme-loader').css('opacity','none');
    //                 // $('.theme-loader').show();
                    
    //                 enviar(editor.getData());
              
    //         }else
    //             aux++;
    //     });
       
    // })
    // .catch( error => {
    //     console.error( 'Oops, something went wrong!' );
    //     console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
    //     console.warn( 'Build id: rpun06eouwm-rzn5b6r6hoyd' );
    //     console.error( error );
    // });

    function verCambio(editor) {
        console.log(aux);
        
    }

    $( document ).ajaxStart(function() {
        $('.theme-loader').css('opacity','none');
        $('.theme-loader').show();
    }); 
 
    $( document ).ajaxStop(function() {
        $('.theme-loader').hide();
    });

	jQuery(document).ready(function($) { 
		cambio();
	});

	function activar_tabla_redaccion() {
    	var t = $('#Tabla-Redaccion').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_redacciones') !!}',
        columns: [
            {data: 'id' },
            {data: 'username' },
            {data: 'nombre' },
            {data: 'created_at' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';
                    var edit = "{{Auth::user()->hasPermissionTo('Redaccion-edit') }}";
                    var remove = "{{Auth::user()->hasPermissionTo('Redaccion-delete') }}";
                    if(edit !='')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="descargar('+row.id+');"><i class="fas fa-wrench"></i> Descargar </a> </br>'; 
                    if(remove !='')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="eliminarArchivo('+row.id+');"><i class="fas fa-wrench"></i> Eliminar </a> </br>'; 

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Redaccion').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    //activar_tabla_redaccion();
    
    function enviar(criterio) {
        if(criterio != '')
        {            
            var id = $(".criterio").val();
            var contenido = criterio;;
            var route = "{{url('guardarTexto')}}";
            var token = $("input[name=_token]").val();
            $.ajax({
                url : route, 
                headers : {'X-CSRF-TOKEN':token},
                method : 'POST',
                dataType : 'json',
                data : {id,contenido},
                success : function (data)
                {   
                    //$('.theme-loader').hide();
                    console.log(data);
                    
                },
                error : function (data){
                    toastr.error("ERROR");
                }
            })
        }
    }


    function abrirRedaccion(criterio)
    {
        var url = "{{url('veredaccion')}}/"+criterio;
        window.open(url, '_blank');
    }

    $("#btnRredaccion").click(function(event) {
        var criterio = $(".criterio").val();
        var users = $("#Rusers").val();
        var route = "{{route('adduser.store')}}";
        var token = $('input[name="_token"]').val();
        $.ajax({
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data:{criterio,users},
            success : function(data){
                $("#modal-Rredaccion").modal('toggle');
                toastr.success("Usuario Asignado");  
            },
            error : function(data){
                toastr.error("ERROR");

            }
        });
    });
   
	function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

    function estilo(){
        $("#Rnombre").prop('disabled', true);
        $(".title-redaccion").html('Agregando Usuarios');
        var route = '{{url("getRedaccion")}}/'+ $(".criterio").val();
        $.get(route, function(data){
            $("#Rnombre").val(data.nombre);
        });
    }

    $('#Rusers').select2({
        placeholder: "Seleccione un Usuario",
        //maximumSelectionSize: 1,
        allowClear: true,
        width: '100%',
        theme: "bootstrap4",
        dropdownParent: $('#modal-Rredaccion'),
        minimumInputLength : 1
    }); 
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
</style>
@endsection