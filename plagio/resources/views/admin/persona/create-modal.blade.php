<div class="modal fade" id="modal-Rpersona" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNpersona','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="staticBackdropLabel" style="margin-left: auto;">Formulario - Nueva Persona</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<div class="form-group"> 
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('nombre','Nombres') !!}
						  	{!! Form::text('nombre',null, 
						  		['class'=>'form-control nombre',
								'placeholder' =>'Nombre de la Persona',
								'id'=>'RPnombre']) !!}
						</div>
						<div class="col-md-6">
						  	{!! Form::label('apellido','Apellidos') !!}
						  	{!! Form::text('apellido',null, 
						  		['class'=>'form-control apellido',
								'placeholder' =>'Apellido de la Persona',
								'id'=>'RPapellido']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('ci','Carnet de Identidad') !!}
						  	{!! Form::text('ci',null, 
						  		['class'=>'form-control ci',
								'placeholder' =>'Carnet de Identidad',
								'id'=>'RPci','onkeypress'=>"return valida(event,true)"]) !!}
						</div>
						<div class="col-md-6">
						  	{!! Form::label('ciudad_id','Extension') !!}
						  	<select class='form-control select_Pciudad' name="ciudad_id" id="Plist_ciudad">
						  		<option value=""></option>
						  	</select> 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{!! Form::label('celular','Nº Telefono') !!}
						  	{!! Form::text('telefono',null, 
						  		['class'=>'form-control telefono',
								'placeholder' =>'+59170045677',
								'id'=>'RPtelefono','onkeypress'=>"return validaTelefono(event,true)"]) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('type','Asignar Usuario') !!}
							<select class='form-control select_Pusuarios' name="user_id" id="Plist_usuarios">
						  		<option value=""></option>
						  	</select>
						</div>						
					</div>
				</div>				 	
				<div class="form-group">
					<div class="row">		
						<div class="col-md-12">
						{!! Form::label('direccion','Direccion') !!}
						{!! Form::text( 
							'direccion',null,
							['class'=>'form-control direccion',
							'placeholder' =>'direccion',
							'id'=>'RPdireccion']) !!}
						</div>
					</div>
				</div>				
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Persona', 
				$attributes = ['id'=>'btnRpersona', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>