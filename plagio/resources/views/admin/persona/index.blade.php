@extends('template.app')

@section('title','Lista Personas')
@section('main-content')
   <!-- Your Page Content Here -->
    @if(Auth::user()->hasPermissionTo('persona-create'))
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info" onclick="llenardatos()" data-toggle="modal" data-target="#modal-Rpersona">
                Nuevo Persona
            </button>
        </div>
    @endif  
    <hr>
    <div class="table-responsive table-striped">
        <table id="Tabla-Personas" class="table table-bordered table-striped" width="100%">
            <thead >
                <tr>
                    <th style="text-align: center;">#</th>
                    <th style="text-align: center;">Nombre Completo</th>
                    <th style="text-align: center;">CI / DNI</th>
                    <th style="text-align: center;">Telefono</th>
                    <th style="text-align: center;">Usuario</th>
                    <th style="text-align: center;">Estado</th>
                    <th style="text-align: center;">Accion</th>
                    
                </tr>
            </thead>
            <tbody style="text-align: center;">

            </tbody>
        </table> 
    </div>

    @include('admin.persona.create-modal')
    @include('admin.persona.edit-modal')

@endsection
@section('js')
<script type="text/javascript">
    $( document ).ready(function() {
        activar_tabla_persona();
        cambio();
        setTimeout(function(){ 
            $(".fa-minus").click(); 
        }, 1000);
    });

    function activar_tabla_persona() {
    var t = $('#Tabla-Personas').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_personas') !!}',
        columns: [
            {data: 'id' },
            {data: 'personal_nombre' },
            {data: 'ci' },
            {data: 'telefono' },
            {data: 'username' },
            {date: 'baja',
                render: function(data, type, row, meta) {
                var html = ''
                if ( row.baja == 'Activo' )
                {
                    html = '<span class="badge badge-success" style="width:80px;"> Activo </span>';
                            
                }else {
                    html = '<span class="badge badge-danger" style="width:80px;"> Inactivo </span>';
                }
                return html;
                }

            },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';
                var editar = "{{Auth::user()->hasPermissionTo('persona-edit') }}";
                
                if(editar != '')
                {                    
                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Epersonal" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';              
                } 
                html +="</div>";
                return html;
            },
        }]
    });
    t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Personas').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 

    function llenardatos()
    {
        llenarCiudades();
        llenarUsuarios();
    } 

    function llenarCiudades()
    {
        var route = "{{url('get_allciudades')}}";
        $.get(route,function(res, sta){
            $("#Plist_ciudad").empty();
            $("#Plist_ciudad").append(`<option value=""> </option>`);
            res.forEach(element => {
                $("#Plist_ciudad").append(`<option value=${element.id}> ${element.nombre} </option>`);
            });
            $("#Plist_ciudad").trigger("chosen:updated");
        });
    }
 
    function llenarUsuarios()
    {
        var route = "{{url('get_usuariosdisponibles')}}";
        $.get(route,function(res, sta){
            $("#Plist_usuarios").empty();
            $("#Plist_usuarios").append(`<option value=""> </option>`);
            res.forEach(element => {
                $("#Plist_usuarios").append(`<option value=${element.id}> ${element.username} </option>`);
            });
            $("#Plist_usuarios").trigger("chosen:updated");
        });
    }

    //Boton para Registrar el Personal
    $("#btnRpersona").click(function(){
        $("#RPnombre").val($("#RPnombre").val().toUpperCase());
        $("#RPapellido").val($("#RPapellido").val().toUpperCase());
        var nombre = $("#RPnombre").val();
        var apellido = $("#RPapellido").val();
        var ci = $("#RPci").val();
        var ciudad_id = $("#Plist_ciudad").val();
        var telefono = $(".telefono").val();
        var direccion = $("#RPdireccion").val();
        var user_id = $("#Plist_usuarios").val()
        var route = "{{route('persona.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:{nombre,apellido,ci,direccion,telefono,ciudad_id,user_id},
        success: function(data){
            $("#modal-Rpersona").modal('toggle');
            toastr.success("Persona "+data.nombre+" Registrado.");  
            $('#Tabla-Personas').DataTable().ajax.reload();
        },
        error:function(data)
        {
            var message="";
            if(data.responseJSON.errors.nombre != undefined){ 
                $('.nombre').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $(".nombre").removeAttr( 'style' );}

            if(data.responseJSON.errors.apellido != undefined){ 
                $('.apellido').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $(".apellido").removeAttr( 'style' );}

            if(data.responseJSON.errors.ci != undefined){ 
                $('.ci').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $(".ci").removeAttr( 'style' );}

            if(data.responseJSON.errors.ciudad_id != undefined){ 
                $('[aria-labelledby="select2-Plist_ciudad-container"]').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $('[aria-labelledby="select2-Plist_ciudad-container"]').removeAttr( 'style' );}

            if(data.responseJSON.errors.direccion != undefined){ 
                $('.direccion').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $(".direccion").removeAttr( 'style' );}
            
            if(data.responseJSON.errors.user_id != undefined){ 
                $('[aria-labelledby="select2-Plist_usuarios-container"]').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $('[aria-labelledby="select2-Plist_usuarios-container"]').removeAttr( 'style' );}

            if(data.responseJSON.errors.telefono != undefined){ 
                $('.telefono').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $(".telefono").removeAttr( 'style' );}

            if($(".telefono").val().length != 0 )
            {
                if(data.responseJSON.errors.telefono!=undefined){
                message = message +  data.responseJSON.errors.telefono[0]+ "<br/>";}
            }
            toastr.error("",message);
        }  
        });
    });

    //llena los datos en el modal para Editar una Personal
    function llenarDatosEdit(id)
    {
        //llenardatosEdit();
        var route = "{{url('persona')}}/"+id+"/edit";
        $.get(route, function(data){
            console.log(data);
            $("#EPid").val(data[0].id);
            $("#EPnombre").val(data[0].nombre);
            $("#EPapellido").val(data[0].apellido);
            $("#EPci").val(data[0].ci);
            $("#EPtelefono").val(data[0].telefono);
            $("#EPlist_ciudad").val(data[0].ciudad_id).trigger("change");
            $("#EPdireccion").val(data[0].direccion);
            $("#EPciudad").val(data[0].ciudad);
            llenarUsuariosEdit(data[0].user_id);
            $("#EPTitle-Label").html("Editando Persona: "+data[0].nombre);
        });
    }

    function llenarUsuariosEdit(id)
    {
        var route = "{{url('get_allUsuarios')}}";
        $.get(route,function(res, sta){
            $("#EPlist_usuarios").empty();
            $("#EPlist_usuarios").append(`<option value=""> </option>`);
            res.forEach(element => {
                if(element.disponible == "SI" || element.id == id)
                    $("#EPlist_usuarios").append(`<option value=${element.id}> ${element.username} </option>`);
            });
            $("#EPlist_usuarios").val(id);
            $("#EPlist_usuarios").trigger("chosen:updated");
        });
    }

    //Actualiza informacion del Personal
    $("#btnEpersona").click(function()
    {
        var id = $("#EPid").val();
        var user_id = $("#EPlist_usuarios").val();
        var ciudad_id = $("#EPlist_ciudad").val();
        var nombre = $("#EPnombre").val();
        var apellido = $("#EPapellido").val();
        var ci = $("#EPci").val();
        var telefono = $("#EPtelefono").val();
        var direccion = $("#EPdireccion").val();

        var route = "{{url('actualizarPersonal')}}/"+id+"";
        var token = $("input[name=_token]").val();
        $.ajax({  
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        method: 'POST',
        ataType: 'json',
        data:{nombre,apellido,ci,telefono,direccion,user_id,ciudad_id},
            success: function(data){
                $("#modal-Epersonal").modal('toggle');
                toastr.success("La Persona: "+data.nombre+" Fue Editada");  
                $('#Tabla-Personas').DataTable().ajax.reload();
            },
            error:function(data)
            {
                var message="";
                toastr.error(message);  
            }  
        });     
    });



    //verficar disponibilidad de un usuario
    function checkDisponible(id)
    {
        var route = "{{url('checkDisponible')}}/"+id;
        $.get(route, function(data){
            console.log(data[0]);
            return data[0];
        });
    }

    //Validacion solo numeros
    var valida = function (event, _float){
        event = event || window.event;
        var charCode = event.keyCode || event.which;
        var first = (charCode <= 57 && charCode >= 48 || charCode==8 || charCode==9 || charCode==11);
        return first;
    }

    var validaTelefono = function (event, _float){
        event = event || window.event;
        var charCode = event.keyCode || event.which;
        var first = (charCode <= 57 && charCode >= 48 || charCode==8 || charCode==9 || charCode==11 || charCode==43);
        return first;
    }

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
</script>
@endsection