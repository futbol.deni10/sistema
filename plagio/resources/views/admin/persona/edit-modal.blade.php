<div class="modal fade" id="modal-Epersonal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FREpersonal','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="EPTitle-Label" style="margin-left: auto;">Formulario - Nuevo Usuario</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body"> 
		  		<input type="hidden" id="EPid">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('nombre','Nombres') !!}
						  	{!! Form::text('nombre',null, 
						  		['class'=>'form-control',
								'id'=>'EPnombre']) !!}
						</div>
						<div class="col-md-6">
						  	{!! Form::label('apellido','Apellidos') !!}
						  	{!! Form::text('apellido',null, 
						  		['class'=>'form-control',
								'id'=>'EPapellido']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('ci','Carnet de Identidad (DNI)') !!}
						  	{!! Form::text('ci',null, 
						  		['class'=>'form-control',
								'id'=>'EPci','onkeypress'=>"return valida(event,true)"]) !!}
						</div> 
						<div class="col-md-6">
						  	{!! Form::label('pais_id','Pais') !!}
						  	<select class='form-control select_Pciudad' style="width: 100%"  id="EPlist_ciudad">
						  		<option value=""></option>
						  		@foreach ($ciudades as $ciudad)
						  		{{-- expr --}}
						  		<option value="{!! $ciudad->id !!}"> 
						  			{!! $ciudad->nombre !!}
						  		</option>
						  		@endforeach
						  	</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{!! Form::label('celular','Nº Telefono') !!}
						  	{!! Form::text('telefono',null, 
						  		['class'=>'form-control telefono',
								'id'=>'EPtelefono','onkeypress'=>"return validaTelefono(event,true)"]) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('type','Asignar Usuario') !!}
							<select class='form-control select_Pusuarios'id="EPlist_usuarios">
								<option value=""></option>
						  	</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">		
						<div class="col-md-12">
						{!! Form::label('direccion','Direccion') !!}
						{!! Form::text( 
							'direccion',null,
							['class'=>'form-control direccion',
							'placeholder' =>'direccion',
							'id'=>'EPdireccion']) !!}
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar Persona', 
				$attributes = ['id'=>'btnEpersona', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>