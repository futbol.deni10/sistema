<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu" >
        <!-- <img src="te.webp" alt=""> -->
        <!-- <div class="pcoded-navigatio-lavel">SISTEMA PLAGIO</div> -->

        <div class="pcoded-navigatio-lavel">USUARIOS</div>
        <ul class="pcoded-item pcoded-left-item">
            @if(Auth::user()->hasPermissionTo('role-list') || Auth::user()->hasPermissionTo('user-list') )
                @if(Auth::user()->hasPermissionTo('role-list'))
                <li class="">
                    <a href="{{route('roles.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-voicemail"></i></span>
                        <span class="pcoded-mtext">Gestor de Roles</span>
                    </a>
                </li>
                @endif
                <!-- if(Auth::user()->role->name == 'Administrador') -->
                @if(Auth::user()->hasPermissionTo('user-list'))
                <li class="">
                    <a href="{{route('users.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                        <span class="pcoded-mtext">Gestor de Usuarios</span>
                    </a>
                </li>
                @endif
               <!--  <li class="">
                    <a href="{{route('persona.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                        <span class="pcoded-mtext">Gestor de Persona</span>
                    </a>
                </li>  -->
                <li class="">
                    <a href="{{route('usuariobloqueado.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-user-x"></i></span>
                        <span class="pcoded-mtext">Usuarios Bloqueados</span>
                    </a>
                </li>               
            @endif
            <div class="pcoded-navigatio-lavel">PLAGIO</div>
            @if(Auth::user()->hasPermissionTo('GestorArchivo-list') || Auth::user()->hasPermissionTo('AlmacenarArchivo-list') || Auth::user()->hasPermissionTo('CompararArchivo-list') || Auth::user()->hasPermissionTo('ResultadosArchivo-list') || Auth::user()->hasPermissionTo('CompararOcr-list'))

                @if(Auth::user()->hasPermissionTo('GestorArchivo-list'))
                <li class="">
                    <a href="{{route('archivo.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-file"></i></span>
                        <span class="pcoded-mtext">Gestor de Archivos</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasPermissionTo('AlmacenarArchivo-list'))
                <li class="">
                    <a href="{{route('repositorio.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-file-plus"></i></span>
                        <span class="pcoded-mtext">Almacenar Archivos</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasPermissionTo('CompararArchivo-list'))
                <li class=" ">
                    <a href="{{route('comparar.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-fast-forward"></i></span>
                        <span class="pcoded-mtext">Comparar Archivos</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasPermissionTo('CompararArchivo-list'))
                <li class=" ">
                    <a href="{{route('ocr.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-image"></i></span>
                        <span class="pcoded-mtext">Comparar OCR</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasPermissionTo('ResultadosArchivo-list'))
                <li class=" ">
                    <a href="{{route('resultado.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-refresh-ccw"></i></span>
                        <span class="pcoded-mtext">Resultados Archivos</span>
                    </a>
                </li>
                @endif

            @endif
            <li class=" ">
                    <a href="{{route('plagionline.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-refresh-ccw"></i></span>
                        <span class="pcoded-mtext">Plagio En Linea</span>
                    </a>
                </li>
            <div class="pcoded-navigatio-lavel">PROYECTOS</div>
            <li class="">
                <a href="{{route('categoriaproyectos.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-layout"></i></span>
                    <span class="pcoded-mtext">Categoria de Proyectos</span>

                </a>
            </li>
            <li class="">
                <a href="{{route('proyectos.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-inbox"></i></span>
                    <span class="pcoded-mtext">Gestor de Proyectos</span>
                </a>
            </li>
            <div class="pcoded-navigatio-lavel">DETALLES DE PROYECTO</div>
            <li class=" ">
                <a href="{{route('asignacion.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Asignar Usuarios</span>
                    
                </a>
            </li>
            <li class=" ">
                <a href="{{route('archivoproyecto.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Gestor Archivos</span>
                    
                </a>
            </li>
            <li class="">
                <a href="{{route('gestionversiones.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-more-vertical"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.default">Gestor de Versiones</span>
                    
                </a>
            </li>
            <li class="">
                <a href="{{route('redaccion.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-edit-2"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.default">Gestor de Redaccion</span>
                    
                </a>
            </li>
            <li class="">
                <a href="{{route('resultadoproyecto.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-refresh-ccw"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.default">Resultados Proyecto</span>                    
                </a>
            </li>
        </ul>
    </div>
</nav>
<style type="text/css">
    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
    }
</style>